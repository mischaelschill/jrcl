package rcl;

import rcl.collections.SafeSet;
import rcl.internal.Entity;
import rcl.internal.FakeProcess;
import rcl.internal.Resource;
import rcl.internal.ThreadStagingArea;

/**
 * This class is used for the RCL transformation and should not be used
 * directly, is it is not available in the dummy library.
 *
 * @author mschill
 */
public final class RCL {

	public static void pass(Object resource, Object controller) {
		if (resource instanceof Resource && controller instanceof Entity) {
			//assert ((Resource) resource).__isWriteable() : "Control violation while passing " + resource;
			if (controller instanceof Entity) {
				((Resource) resource).__pass((Entity) controller);
			} else if (controller instanceof Thread) {
				((Resource) resource).__pass(FakeProcess.getCurrentOrFakeProcess());
			}
		}
	}

	public static void checkRead(Object resource) {
		if (resource instanceof Resource) {
			assert ((Resource) resource).__isReadable() : "Control violation";
		}
	}

	public static void checkWrite(Object resource) {
		if (resource instanceof Resource) {
			assert ((Resource) resource).__isWriteable() : "Control violation";
		}
	}
	
	public static void __assign(Object modified, Object newValue) {
		if (newValue instanceof Resource && modified instanceof Entity) {
			Resource v = (Resource) newValue;
			if (((Object)v.__getController()) instanceof ThreadStagingArea) {
				Entity m = (Entity) modified;
				while (m instanceof Resource && m != newValue) {
					m = ((Resource)m).__getController();
				}
				if (!(m instanceof Resource)) {
					v.__setControl((Entity)modified);
//					System.err.println ("passed " + v + " to " + modified);
//					for (StackTraceElement e : Thread.currentThread().getStackTrace()) {
//						System.err.println(e.toString());
//					}
				}
			}
		}
	}
}
