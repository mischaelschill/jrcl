package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import rcl.annotations.Shared;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import rcl.internal.Entity;
import rcl.internal.FakeProcess;
import rcl.internal.SharedResource;

/**
 *
 * @author mischael
 */
@AbstractProcess
public final class ReadWriteResourceLock<T> {

	private final SharedBox<T> reader;
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	@SuppressWarnings("LeakingThisInConstructor")
	public ReadWriteResourceLock(T content) {
		reader = new SharedBox<T>(content);
	}

	public T getContent() {
		return reader.getContent();
	}

	public void lockWrite() {
		lock.writeLock().lock();
		Thread t = Thread.currentThread();
		if (t instanceof Entity) {
			RCL.pass(reader, (Entity) t);
		} else {
			RCL.pass(reader, FakeProcess.getFakeProcess());
		}
	}

	public void unlockWrite() {
		RCL.pass(reader, this);
		lock.writeLock().unlock();
	}

	public void lockRead() {
		lock.readLock().lock();
		((SharedResource) reader).__share(FakeProcess.getFakeProcess());
	}

	public void unlockRead() {
		((SharedResource) reader).__release();
		lock.readLock().unlock();
	}

	public boolean __isWriteable() {
		return lock.writeLock().isHeldByCurrentThread();
	}

	public boolean __isReadable() {
		return __isWriteable();
	}
}

@Shared
class SharedBox<T> {

	private final T content;

	public SharedBox(T content) {
		this.content = content;
	}

	public T getContent() {
		return content;
	}
}
