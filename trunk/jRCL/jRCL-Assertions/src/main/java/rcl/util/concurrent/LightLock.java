package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import rcl.internal.Entity;
import rcl.internal.FakeProcess;

/**
 * @author mischael
 * @param <T> The type of the object to protect
 */
@AbstractProcess
public class LightLock<T> implements Lock {

	private final T content;
	private final Semaphore lock = new Semaphore(1);
	private final ThreadLocal<Boolean> inMutex = new ThreadLocal<>();

	@SuppressWarnings("LeakingThisInConstructor")
	public LightLock(T content) {
		assert content != null;
		RCL.pass(content, this);
		this.content = content;
	}

	/**
	 * @return the object that is protected by the lock
	 */
	public T getContent() {
		return content;
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lock() {
		lock.acquireUninterruptibly();
		inMutex.set(true);
		RCL.pass(content, getCurrentProcess());
		inMutex.set(null);
	}

	/**
	 * Releases the lock: Transfers control of the content to the lock
	 */
	@Override
	public void unlock() {
		RCL.pass(content, this);
		lock.release();
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lockInterruptibly() throws InterruptedException {
		lock.acquire();
		inMutex.set(true);
		RCL.pass(content, getCurrentProcess());
		inMutex.set(null);
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock() {
		boolean retval = lock.tryAcquire();
		if (retval) {
			inMutex.set(true);
			RCL.pass(content, getCurrentProcess());
			inMutex.set(null);
		}
		return retval;
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @param time the number of time units to wait until giving up
	 * @param unit the time unit
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		boolean retval = lock.tryAcquire(time, unit);
		if (retval) {
			inMutex.set(true);
			RCL.pass(content, getCurrentProcess());
			inMutex.set(null);
		}
		return retval;
	}

	public boolean __isWriteable() {
		return inMutex.get() != null;
	}

	public boolean __isReadable() {
		return __isWriteable();
	}

	private Entity getCurrentProcess() {
		Thread t = Thread.currentThread();
		if (t instanceof Entity) {
			return (Entity) t;
		}
		return FakeProcess.getFakeProcess();
	}

	@Override
	public Condition newCondition() {
		throw new UnsupportedOperationException("Not supported by light locks.");
	}
}
