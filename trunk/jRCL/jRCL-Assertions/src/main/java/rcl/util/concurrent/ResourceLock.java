package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import rcl.internal.Entity;
import rcl.internal.FakeProcess;
import rcl.internal.Resource;

/**
 *
 * @author mischael
 * @param <T> The type of the object to protect
 */
@AbstractProcess
public class ResourceLock<T> implements Lock {

	private final T content;
	private final ReentrantLock lock = new ReentrantLock();

	public ResourceLock(T content) {
		//assert content != null;
		//assert RCL.isWriteable(content) : "Creation of a resource lock without owning the content";
		this(content, false);
	}

	@SuppressWarnings("LeakingThisInConstructor")
	public ResourceLock(T content, boolean locked) {
		//assert content != null;
		//assert RCL.isWriteable(content) : "Creation of a resource lock without owning the content";
		if (locked) {
			lock();
		} else {
			RCL.pass(content, this);
		}
		this.content = content;
	}

	/**
	 * @return the object that is protected by the lock
	 */
	public T getContent() {
		return content;
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lock() {
		lock.lock();
		//TODO: Change Thread.currentThread() to the caller
		RCL.pass(content, getCurrentProcess());
	}

	/**
	 * Releases the lock: Transfers control of the content to the lock
	 */
	@Override
	public void unlock() {
		RCL.pass(content, this);
		lock.unlock();
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lockInterruptibly() throws InterruptedException {
		lock.lockInterruptibly();
		//TODO: Change Thread.currentThread() to the caller
		RCL.pass(content, getCurrentProcess());
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock() {
		boolean retval = lock.tryLock();
		//TODO: Change Thread.currentThread() to the caller
		if (retval) {
			RCL.pass(content, getCurrentProcess());
		}
		return retval;
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @param time the number of time units to wait until giving up
	 * @param unit the time unit
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		boolean retval = lock.tryLock(time, unit);
		//TODO: Change Thread.currentThread() to the caller
		if (retval) {
			RCL.pass(content, getCurrentProcess());
		}
		return retval;
	}

	public boolean isHeldByCurrentThread() {
		return lock.isHeldByCurrentThread();
	}

	public boolean isLocked() {
		return lock.isLocked();
	}
	
	public boolean __isWriteable() {
		return isHeldByCurrentThread();
	}

	public boolean __isReadable() {
		return __isWriteable();
	}

	@AbstractProcess
	class CheckedCondition implements Condition {

		private final Condition cond = lock.newCondition();

		@Override
		public void await() throws InterruptedException {
			RCL.pass(content, this);
			cond.await();
			//TODO: Change Thread.currentThread() to the caller
			RCL.pass(content, getCurrentProcess());
		}

		@Override
		public void awaitUninterruptibly() {
			RCL.pass(content, this);
			cond.awaitUninterruptibly();
			//TODO: Change Thread.currentThread() to the caller
			RCL.pass(content, getCurrentProcess());
		}

		@Override
		public long awaitNanos(long nanosTimeout) throws InterruptedException {
			RCL.pass(content, this);
			long retval = cond.awaitNanos(nanosTimeout);
			//TODO: Change Thread.currentThread() to the caller
			RCL.pass(content, getCurrentProcess());
			return retval;
		}

		@Override
		public boolean await(long time, TimeUnit unit) throws InterruptedException {
			RCL.pass(content, this);
			boolean retval = cond.await(time, unit);
			//TODO: Change Thread.currentThread() to the caller
			RCL.pass(content, getCurrentProcess());
			return retval;
		}

		@Override
		public boolean awaitUntil(Date deadline) throws InterruptedException {
			RCL.pass(content, this);
			boolean retval = cond.awaitUntil(deadline);
			//TODO: Change Thread.currentThread() to the caller
			RCL.pass(content, getCurrentProcess());
			return retval;
		}

		@Override
		public void signal() {
			cond.signal();
		}

		@Override
		public void signalAll() {
			cond.signalAll();
		}

		public boolean __isWriteable() {
			return isHeldByCurrentThread();
		}

		public boolean __isReadable() {
			return __isWriteable();
		}
	}

	/**
	 * @return a new condition variable
	 */
	@Override
	public Condition newCondition() {
		return new CheckedCondition();
	}

	private Entity getCurrentProcess() {
		Thread t = Thread.currentThread();
		if (t instanceof Entity) {
			return (Entity) t;
		}
		return FakeProcess.getFakeProcess();
	}
}
