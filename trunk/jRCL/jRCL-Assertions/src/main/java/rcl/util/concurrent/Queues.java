package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import rcl.collections.SafeIterator;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import rcl.internal.Entity;
import rcl.internal.FakeProcess;
import rcl.internal.Resource;

/**
 *
 * @author mschill
 */
public class Queues {
	public static <E> BlockingQueue<E> fromBlockingQueue(BlockingQueue<E> queue) {
		return new SafeBlockingQueue<>(queue);
	}
}

@AbstractProcess
class SafeBlockingQueue<E> implements BlockingQueue<E> {
	private final BlockingQueue<E> queue;
	private final ThreadLocal<Boolean> inQueue = new ThreadLocal<>();
	
	public SafeBlockingQueue(BlockingQueue<E> queue) {
		this.queue = queue;
	}

	@Override
	public boolean add(E e) {
		boolean b = queue.add(e);
		RCL.pass(e, this);
		return b;
	}

	@Override
	public boolean offer(E e) {
		boolean b = queue.offer(e);
		if (b) {
			RCL.pass(e, this);
		}
		return b;
	}

	@Override
	public E remove() {
		E e = queue.remove();
		inQueue.set(true);
		RCL.pass(e, getCurrentProcess());
		inQueue.set(false);
		return e;
	}

	@Override
	public E poll() {
		E e = queue.poll();
		if (e != null) {
			inQueue.set(true);
			RCL.pass(e, getCurrentProcess());
			inQueue.set(false);
		}
		return e;
	}

	@Override
	public E element() {
		return queue.element();
	}

	@Override
	public E peek() {
		return queue.peek();
	}

	@Override
	public int size() {
		return queue.size();
	}

	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return queue.contains(o);
	}

	@Override
	public Iterator<E> iterator() {
		return new SafeIterator<>(queue.iterator(), queue);
	}

	@Override
	public Object[] toArray() {
		return queue.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return queue.toArray(a);
	}

	@Override
	public boolean remove(Object o) {
		return queue.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return queue.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		for (Object s : c) {
			RCL.pass(s, this);
		}
		return queue.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return queue.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return queue.retainAll(c);
	}

	@Override
	public void clear() {
		queue.clear();
	}

	@Override
	public int hashCode() {
		return queue.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SafeBlockingQueue) {
			return queue.equals(obj);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return queue.toString();
	}

	@Override
	public void put(E e) throws InterruptedException {
		RCL.pass(e, this);
		queue.put(e);
	}

	@Override
	public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
		Object s = ((Resource)e).__getController();
		RCL.pass(e, this);
		boolean b = queue.offer(e, timeout, unit);
		if (!b) {
			inQueue.set(true);
			RCL.pass(e, getCurrentProcess());
			inQueue.set(false);
		}
		return b;
	}

	@Override
	public E take() throws InterruptedException {
		E e = queue.take();
		inQueue.set(true);
		RCL.pass(e, getCurrentProcess());
		inQueue.set(false);
		return e;
	}

	@Override
	public E poll(long timeout, TimeUnit unit) throws InterruptedException {
		E e = queue.poll(timeout, unit);
		if (e != null) {
			inQueue.set(true);
			RCL.pass(e, getCurrentProcess());
			inQueue.set(false);
		}
		return e;
	}

	@Override
	public int remainingCapacity() {
		return queue.remainingCapacity();
	}

	@Override
	public int drainTo(Collection<? super E> c) {
		return queue.drainTo(c);
	}

	@Override
	public int drainTo(Collection<? super E> c, int maxElements) {
		return queue.drainTo(c, maxElements);
	}
	
	public boolean __isWriteable() {
		return inQueue.get() == true;
	}
	
	public boolean __isReadable() {
		return __isWriteable();
	}
	
	private Entity getCurrentProcess() {
		Thread t = Thread.currentThread();
		if (t instanceof Entity)
			return (Entity)t;
		return FakeProcess.getFakeProcess();
	}
}