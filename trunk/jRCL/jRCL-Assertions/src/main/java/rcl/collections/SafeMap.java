/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SafeMap<K, V> implements Map<K, V> {

	private final Map<K, V> original;

	public SafeMap(Map<K, V> original) {
		this.original = original;
	}

	@Override
	public int size() {
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		return original.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return original.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return original.containsValue(value);
	}

	@Override
	public V get(Object key) {
		return original.get(key);
	}

	@Override
	public V put(K key, V value) {
		return original.put(key, value);
	}

	@Override
	public V remove(Object key) {
		return original.remove(key);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		original.putAll(m);
	}

	@Override
	public void clear() {
		original.clear();
	}

	private Set<K> keySet;

	@Override
	public Set<K> keySet() {
		if (keySet == null) {
			keySet = new SafeSet<>(original.keySet(), this);
		}
		return keySet;
	}

	private Collection<V> values;
	
	@Override
	public Collection<V> values() {
		return original.values();
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return original.entrySet();
	}

	@Override
	public boolean equals(Object o) {
		return original.equals(o);
	}

	@Override
	public int hashCode() {
		return original.hashCode();
	}

	@Override
	public String toString() {
		return original.toString();
	}

}
