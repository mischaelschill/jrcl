/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rcl.collections;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import rcl.RCL;
import rcl.internal.Resource;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SafeDeque<T> implements Deque<T> {
	private final Deque<T> original;
	private final Object source;
	
	public SafeDeque (Deque<T> original) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	public SafeDeque (Deque<T> original, Object source) {
		this.original = original;
		this.source = source;
	}

	@Override
	public void addFirst(T e) {
		RCL.checkWrite(source);
		original.addFirst(e);
	}

	@Override
	public void addLast(T e) {
		RCL.checkWrite(source);
		original.addLast(e);
	}

	@Override
	public boolean offerFirst(T e) {
		RCL.checkWrite(source);
		return original.offerFirst(e);
	}

	@Override
	public boolean offerLast(T e) {
		RCL.checkWrite(source);
		return original.offerLast(e);
	}

	@Override
	public T removeFirst() {
		RCL.checkWrite(source);
		return original.removeFirst();
	}

	@Override
	public T removeLast() {
		RCL.checkWrite(source);
		return original.removeLast();
	}

	@Override
	public T pollFirst() {
		RCL.checkWrite(source);
		return original.pollFirst();
	}

	@Override
	public T pollLast() {
		RCL.checkWrite(source);
		return original.pollLast();
	}

	@Override
	public T getFirst() {
		RCL.checkRead(source);
		return original.getFirst();
	}

	@Override
	public T getLast() {
		RCL.checkRead(source);
		return original.getLast();
	}

	@Override
	public T peekFirst() {
		RCL.checkRead(source);
		return original.peekFirst();
	}

	@Override
	public T peekLast() {
		RCL.checkRead(source);
		return original.peekLast();
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		RCL.checkWrite(source);
		return original.removeFirstOccurrence(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		RCL.checkWrite(source);
		return original.removeLastOccurrence(o);
	}

	@Override
	public boolean add(T e) {
		RCL.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean offer(T e) {
		RCL.checkWrite(source);
		return original.offer(e);
	}

	@Override
	public T remove() {
		RCL.checkWrite(source);
		return original.remove();
	}

	@Override
	public T poll() {
		RCL.checkWrite(source);
		return original.poll();
	}

	@Override
	public T element() {
		RCL.checkRead(source);
		return original.element();
	}

	@Override
	public T peek() {
		RCL.checkRead(source);
		return original.peek();
	}

	@Override
	public void push(T e) {
		RCL.checkWrite(source);
		original.push(e);
	}

	@Override
	public T pop() {
		RCL.checkWrite(source);
		return original.pop();
	}

	@Override
	public boolean remove(Object o) {
		RCL.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean contains(Object o) {
		RCL.checkRead(source);
		return original.contains(o);
	}

	@Override
	public int size() {
		RCL.checkRead(source);
		return original.size();
	}

	@Override
	public Iterator<T> iterator() {
		RCL.checkRead(source);
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Iterator<T> descendingIterator() {
		RCL.checkRead(source);
		return new SafeIterator<>(original.descendingIterator(), source);
	}

	@Override
	public boolean isEmpty() {
		RCL.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public Object[] toArray() {
		RCL.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		RCL.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		RCL.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		RCL.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		RCL.checkWrite(source);
		original.clear();
	}

	@Override
	public int hashCode() {
		RCL.checkRead(source);
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		RCL.checkRead(source);
		RCL.checkRead(obj);
		if (obj instanceof SafeDeque) {
			RCL.checkRead(((SafeDeque)obj).original);
			return original.equals(((SafeDeque)obj).original);
		} else return original.equals(obj);
	}

	@Override
	public String toString() {
		RCL.checkRead(source);
		return original.toString();
	}
}
