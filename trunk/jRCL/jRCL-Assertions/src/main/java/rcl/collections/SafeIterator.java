package rcl.collections;

import rcl.annotations.Checked;
import java.util.Iterator;
import rcl.RCL;

/**
 *
 * @author mischael
 */
@Checked
public class SafeIterator<T> implements Iterator<T> {
	private final Iterator<T> original;
	private final Object collection;

	public SafeIterator(Iterator<T> original, Object collection) {
		this.original = original;
		this.collection = collection;
	}

	@Override
	public boolean hasNext() {
		RCL.checkRead (collection);
		return original.hasNext();
	}

	@Override
	public T next() {
		RCL.checkRead (collection);
		return original.next();
	}

	@Override
	public void remove() {
		RCL.checkWrite (collection);
		original.remove();
	}
}
