package rcl.collections;

import rcl.annotations.Checked;
import java.util.Collection;
import java.util.Set;
import rcl.RCL;
import rcl.internal.Resource;

/**
 *
 * @author mischael
 */
@Checked
public class SafeSet<T> implements Set<T> {
	private final Set<T> original;

	public SafeSet(Set<T> original) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	private final Object source;
	
	SafeSet(Set<T> original, Object source) {
		this.original = original;
		this.source = source;
	}

	
	@Override
	public int size() {
		RCL.checkRead(source);
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		RCL.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		RCL.checkRead(source);
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<T>(original.iterator(), source);
	}

	@Override
	public Object[] toArray() {
		RCL.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		RCL.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		RCL.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		RCL.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		RCL.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		RCL.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public void clear() {
		RCL.checkWrite(source);
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		RCL.checkRead(source);
		RCL.checkRead(o);
		if (o instanceof SafeSet) {
			RCL.checkRead(((SafeSet)o).original);
			return original.equals(((SafeSet)o).original);
		} else {
			return original.equals(o);
		}
	}

	@Override
	public int hashCode() {
		RCL.checkRead(source);
		return original.hashCode();
	}

	@Override
	public String toString() {
		RCL.checkRead(source);
		return original.toString();
	}
	
}
