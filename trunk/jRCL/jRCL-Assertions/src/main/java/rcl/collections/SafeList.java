package rcl.collections;

import rcl.annotations.Checked;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import rcl.RCL;
import rcl.internal.Resource;

/**
 *
 * @author mischael
 */
@Checked
public class SafeList<T> implements List<T> {

	private final List<T> original;
	private final Object source;

	public SafeList(List<T> original) {
		assert original != null;
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	SafeList(List<T> original, Object source) {
		assert original != null;
		assert source != null;
		this.original = original;
		this.source = source;
	}
	
	@Override
	public int size() {
		RCL.checkRead(source);
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		RCL.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		RCL.checkRead(source);
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Object[] toArray() {
		RCL.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		RCL.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		RCL.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		RCL.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		RCL.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		RCL.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		RCL.checkWrite(source);
		return original.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		RCL.checkWrite(source);
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		RCL.checkRead(source);
		RCL.checkRead(o);
		if (o instanceof SafeList) {
			SafeList t = (SafeList) o;
			RCL.checkRead(t.original);
			if (original == null) {
				return t.original == null;
			}
			return original.equals(t.original);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		RCL.checkRead(source);
		if (original == null) {
			return super.hashCode();
		}
		return original.hashCode();
	}

	@Override
	public T get(int index) {
		RCL.checkRead(source);
		return original.get(index);
	}

	@Override
	public T set(int index, T element) {
		RCL.checkWrite(source);
		return original.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		RCL.checkWrite(source);
		original.add(index, element);
	}

	@Override
	public T remove(int index) {
		RCL.checkWrite(source);
		return original.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		RCL.checkRead(source);
		return original.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		RCL.checkRead(source);
		return original.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		RCL.checkRead(source);
		return new SafeListIterator<>(original.listIterator(), source);
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		RCL.checkRead(source);
		return new SafeListIterator<>(original.listIterator(index), source);
	}

	@Override
	public SafeList<T> subList(int fromIndex, int toIndex) {
		RCL.checkRead(source);
		return new SafeList<>(original.subList(fromIndex, toIndex), source);
	}

	@Override
	public String toString() {
		RCL.checkRead(source);
		return original.toString();
	}

}
