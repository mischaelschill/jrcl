package rcl.collections;

import rcl.annotations.Checked;
import java.util.ListIterator;
import rcl.RCL;
import rcl.internal.Resource;

/**
 *
 * @author mischael
 */
@Checked
class SafeListIterator<T> implements ListIterator<T> {
	private final ListIterator<T> original;
	private final Object source;

	public SafeListIterator(ListIterator<T> original, Object source) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	@Override
	public boolean hasNext() {
		RCL.checkRead(source);
		return original.hasNext();
	}

	@Override
	public T next() {
		RCL.checkRead(source);
		return original.next();
	}

	@Override
	public boolean hasPrevious() {
		RCL.checkRead(source);
		return original.hasPrevious();
	}

	@Override
	public T previous() {
		RCL.checkRead(source);
		return original.previous();
	}

	@Override
	public int nextIndex() {
		RCL.checkRead(source);
		return original.nextIndex();
	}

	@Override
	public int previousIndex() {
		RCL.checkRead(source);
		return original.previousIndex();
	}

	@Override
	public void remove() {
		RCL.checkWrite(source);
		original.remove();
	}

	@Override
	public void set(T e) {
		RCL.checkWrite(source);
		original.set(e);
	}

	@Override
	public void add(T e) {
		RCL.checkWrite(source);
		original.add(e);
	}

	@Override
	public int hashCode() {
		RCL.checkRead(source);
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		RCL.checkRead(source);
		if (obj instanceof SafeListIterator){
			RCL.checkRead(((SafeListIterator)obj).original);
			return original.equals(((SafeListIterator)obj).original);
		} else {
			return original.equals(obj);
		}
	}

	@Override
	public String toString() {
		RCL.checkRead(source);
		return original.toString();
	}
	
}
