/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.util.Arrays;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.*;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class ProcessClassVisitor extends CheckedClassVisitor {

	private String name;

	public ProcessClassVisitor(ClassVisitor cv, TransformationContext context) {
		super(cv, context);
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.name = name;
		if (context.getClassification(superName) == ClassClassification.UNCHECKED_PROCESS) {
			interfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
			interfaces[interfaces.length - 1] = "rcl/internal/Process";
		}

		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	protected void generateRCLMethods() {

		{
			FieldVisitor fv = visitField(ACC_PRIVATE + ACC_SYNTHETIC, "__controller", "Lrcl/internal/Entity;", null, null);
			fv.visitEnd();
		}
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC, "__isWriteable", "()Z", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;");
			Label l0 = new Label();
			mv.visitJumpInsn(IF_ACMPEQ, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "isAlive", "()Z");
			Label l1 = new Label();
			mv.visitJumpInsn(IFNE, l1);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, name, "__controller", "Lrcl/internal/Entity;");
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
			mv.visitJumpInsn(IFEQ, l1);
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitInsn(ICONST_1);
			Label l2 = new Label();
			mv.visitJumpInsn(GOTO, l2);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitInsn(ICONST_0);
			mv.visitLabel(l2);
			mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			mv.visitInsn(IRETURN);
			mv.visitMaxs(2, 1);
			mv.visitEnd();
		}
		
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC, "__isReadable", "()Z", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;");
			Label l0 = new Label();
			mv.visitJumpInsn(IF_ACMPEQ, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "isAlive", "()Z");
			Label l1 = new Label();
			mv.visitJumpInsn(IFNE, l1);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, name, "__controller", "Lrcl/internal/Entity;");
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isReadable", "()Z");
			mv.visitJumpInsn(IFEQ, l1);
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitInsn(ICONST_1);
			Label l2 = new Label();
			mv.visitJumpInsn(GOTO, l2);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitInsn(ICONST_0);
			mv.visitLabel(l2);
			mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			mv.visitInsn(IRETURN);
			mv.visitMaxs(2, 1);
			mv.visitEnd();
		}
	}
}
