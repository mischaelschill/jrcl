/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.util.ArrayDeque;
import java.util.Arrays;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ACC_SYNCHRONIZED;
import static org.objectweb.asm.Opcodes.ACONST_NULL;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.ASM4;
import static org.objectweb.asm.Opcodes.ASTORE;
import static org.objectweb.asm.Opcodes.ATHROW;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.DUP2;
import static org.objectweb.asm.Opcodes.DUP2_X1;
import static org.objectweb.asm.Opcodes.DUP_X2;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.INSTANCEOF;
import static org.objectweb.asm.Opcodes.INVOKEINTERFACE;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.MONITORENTER;
import static org.objectweb.asm.Opcodes.MONITOREXIT;
import static org.objectweb.asm.Opcodes.NEW;
import static org.objectweb.asm.Opcodes.POP;
import static org.objectweb.asm.Opcodes.POP2;
import static org.objectweb.asm.Opcodes.PUTFIELD;
import static org.objectweb.asm.Opcodes.RETURN;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;
import rcl.internal.Entity;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class CheckedMethodVisitor extends MethodVisitor implements Opcodes {

	//Method name
	protected final String name;

	protected final String desc;

	protected final String classType;

	protected final TransformationContext context;

	private boolean firstConstructorVisited;

	private final int lastArgument;

	private final int access;

	private LocalVariablesSorter sorter;

	private final String superClass;

//	private final LinkedList<Integer> passedParameters = new LinkedList<>();
	public CheckedMethodVisitor(MethodVisitor mv, int access, String name,
			String desc, String classType, TransformationContext context,
			String superClass) {
		super(ASM4, mv);
		// Remember the method name
		this.name = name;
		this.desc = desc;
		this.classType = classType;
		this.context = context;
		this.superClass = superClass;
		firstConstructorVisited = !name.equals("<init>");
		int sizes = 0;
		for (Type t : Type.getArgumentTypes(this.desc)) {
			sizes += t.getSize();
		}
		lastArgument = sizes;
		this.access = access;
	}

	public void setSorter(LocalVariablesSorter sorter) {
		this.sorter = sorter;
	}

//	@Override
//	public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
//		if (Type.getType(desc).equals(Type.getObjectType("ch/ethz/inf/se/rcl/annotations/Pass"))) {
//			Transformer.returnPassingMethods.add(className+name+desc);
//		}
//		return super.visitAnnotation(desc, visible);
//	}
//
//	@Override
//	public AnnotationVisitor visitParameterAnnotation(int parameter, String desc, boolean visible) {
//		if (Type.getType(desc).getClassName().equals(Pass.class.getName())) {
//			passedParameters.add(parameter);
//		}
//		return super.visitParameterAnnotation(parameter, desc, visible); //To change body of generated methods, choose Tools | Templates.
//	}
//
//	private void generateParameterPassingCode() {
//		for (int p : passedParameters) {
//			super.visitVarInsn(ALOAD, p);
//			super.visitVarInsn(ALOAD, 0);
//			super.visitMethodInsn(INVOKEVIRTUAL, "rcl/internal/Entity",
//					"__pass",
//					Type.getMethodType(
//							Type.VOID_TYPE,
//							Type.getObjectType("rcl/internal/Entity")
//					).toString()
//			);
//		}
//	}
//
//	private void generateReturnPassingCode() {
//		super.visitInsn(DUP);
//		super.visitVarInsn(ALOAD, 0);
//		super.visitMethodInsn(INVOKEVIRTUAL, "rcl/internal/Entity",
//				"__pass",
//				Type.getMethodType(
//						Type.VOID_TYPE,
//						Type.getObjectType("rcl/internal/Entity")
//				).toString()
//		);
//	}
	ArrayDeque<Integer> monitorLocals = new ArrayDeque<>();

//	@Override
//	public void visitMethodInsn(int opcode, String owner, String name, String desc) {
//		if (name.equals("<init>")) {
//			ClassClassification c = context.getClassification(owner);
//			if (c == ClassClassification.RESOURCE || c == ClassClassification.PROCESS) {
//				if (firstConstructorVisited) {
//					super.visitVarInsn(ALOAD, 0);
//				} else {
//					firstConstructorVisited = true;
//					super.visitVarInsn(ALOAD, lastArgument);
//				}
//				Type[] arguments = Type.getArgumentTypes(desc);
//				arguments = Arrays.copyOf(arguments, arguments.length + 1);
//				arguments[arguments.length - 1] = Type.getObjectType("rcl/internal/Entity");
//				Type newType = Type.getMethodType(Type.VOID_TYPE, arguments);
//				super.visitMethodInsn(opcode, owner, name, newType.getDescriptor());
//			} else {
//				//A NON-RCL object constructor
//				super.visitMethodInsn(opcode, owner, name, desc);
//				if (!firstConstructorVisited) {
//					firstConstructorVisited = true;
//					generateControllerInitializationCode(lastArgument);
//				}
//			}
////		} else if (Transformer.returnPassingMethods.contains(owner+name+desc)) {
////			generateReturnPassingCode();
//		} else {
//			//A regular method
//			super.visitMethodInsn(opcode, owner, name, desc);
//		}
//	}
	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String desc) {
		super.visitMethodInsn(opcode, owner, name, desc);
		if (name.equals("<init>")) {
			ClassClassification c = context.getClassification(owner);

			if (c != ClassClassification.RESOURCE && c != ClassClassification.PROCESS && !firstConstructorVisited && owner.equals(superClass)) {
				generateControllerInitializationCode();
				firstConstructorVisited = true;
			}
		}
	}

	@Override
	public void visitCode() {
		super.visitCode();
		if ((access & ACC_SYNCHRONIZED) != 0) {
			//TOS:
			int local = sorter.newLocal(Type.getObjectType("rcl/internal/Entity"));
			super.visitInsn(ACONST_NULL);
			//TOS: null
			super.visitVarInsn(ASTORE, local);
			//TOS:
			super.visitVarInsn(ALOAD, 0);
			//TOS: object
			super.visitTypeInsn(INSTANCEOF, "rcl/internal/Resource");
			//TOS: int
			Label finishLabel = new Label();
			super.visitJumpInsn(IFEQ, finishLabel);
			//TOS: 
			super.visitVarInsn(ALOAD, 0);
			//TOS: object
			super.visitMethodInsn(INVOKESTATIC, "rcl/internal/FakeProcess", "getCurrentOrFakeProcess", "()Lrcl/internal/Process;");
			//TOS: object, currentProcess
			super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Resource", "__setControl", Type.getMethodType(Type.getType(Entity.class), Type.getType(Entity.class)).getDescriptor());
			//TOS: oldController
			super.visitVarInsn(ASTORE, local);
			//TOS: 
			//Remember the local variable that holds the old controller
			monitorLocals.push(local);
			super.visitLabel(finishLabel);
			//TOS: 
		}
	}

	@Override
	public void visitInsn(int opcode) {
		Label skipLabel = null;
		switch (opcode) {

			case MONITORENTER:
				//TOS: object
				super.visitInsn(DUP);
				//TOS: object, object
				//Enter the monitor
				super.visitInsn(opcode);
				//TOS: object
				super.visitInsn(DUP);
				//TOS: object, object
				super.visitTypeInsn(INSTANCEOF, "rcl/internal/Resource");
				//TOS: int, object
				int local = sorter.newLocal(Type.getObjectType("rcl/internal/Entity"));
				super.visitInsn(ACONST_NULL);
				//TOS: int, object, null			
				super.visitVarInsn(ASTORE, local);
				//TOS: int, object
				skipLabel = new Label();
				super.visitJumpInsn(IFEQ, skipLabel);
				//TOS: object
				super.visitMethodInsn(INVOKESTATIC, "rcl/internal/FakeProcess", "getCurrentOrFakeProcess", "()Lrcl/internal/Process;");
				//TOS: object, currentProcess
				super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Resource", "__setControl", Type.getMethodType(Type.getType(Entity.class), Type.getType(Entity.class)).getDescriptor());
				//TOS: oldController
				super.visitVarInsn(ASTORE, local);
				//TOS: 
				//Remember the local variable that holds the old controller
				monitorLocals.push(local);
				monitorLocals.push(local);
				Label finishLabel = new Label();
				super.visitJumpInsn(GOTO, finishLabel);
				super.visitLabel(skipLabel);
				//TOS: object
				super.visitInsn(POP);
				//TOS: 
				super.visitLabel(finishLabel);
				//TOS: 
				break;
			case MONITOREXIT:
				//TOS: object
				super.visitInsn(DUP);
				//TOS: object, object
				super.visitTypeInsn(INSTANCEOF, "rcl/internal/Resource");
				//TOS: int, object
				skipLabel = new Label();
				super.visitJumpInsn(IFEQ, skipLabel);
				//TOS: object
				super.visitInsn(DUP);
				//TOS: object, object
				super.visitVarInsn(ALOAD, monitorLocals.pop());
				//TOS: object, object, oldcontroller
				super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Resource", "__setControl", Type.getMethodType(Type.getType(Entity.class), Type.getType(Entity.class)).getDescriptor());
				//TOS: object, lastcontroller
				super.visitInsn(POP);
				//TOS: object
				super.visitLabel(skipLabel);
				//TOS: object
				//Exit the monitor
				super.visitInsn(opcode);
				//TOS:
				break;
			case RETURN:
			case ARETURN:
			case LRETURN:
			case FRETURN:
			case DRETURN:
			case IRETURN:
				if ((access & ACC_SYNCHRONIZED) != 0) {
					super.visitVarInsn(ALOAD, 0);
					//TOS: object
					super.visitTypeInsn(INSTANCEOF, "rcl/internal/Resource");
					//TOS: int
					skipLabel = new Label();
					super.visitJumpInsn(IFEQ, skipLabel);
					//TOS: 
					super.visitVarInsn(ALOAD, 0);
					//TOS: object
					super.visitVarInsn(ALOAD, monitorLocals.pop());
					//TOS: object, oldcontroller
					super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Resource", "__setControl", Type.getMethodType(Type.getType(Entity.class), Type.getType(Entity.class)).getDescriptor());
					//TOS: lastcontroller
					super.visitInsn(POP);
					//TOS:
					super.visitLabel(skipLabel);
					//TOS:
				}
			default:
				super.visitInsn(opcode);
		}
	}

//	protected void generateControllerInitializationCode(int callerArgument) {
//		super.visitVarInsn(ALOAD, 0);
//		super.visitVarInsn(ALOAD, callerArgument);
//		super.visitFieldInsn(PUTFIELD, classType, "__controller", "Lrcl/internal/Entity;");
//	}
	protected void generateControllerInitializationCode() {
		if (classType.equals("rcl/internal/ThreadStagingArea")) {
			super.visitVarInsn(ALOAD, 0);
			super.visitMethodInsn(INVOKESTATIC, "rcl/internal/FakeProcess", "getCurrentOrFakeProcess", "()Lrcl/internal/Process;");
			super.visitFieldInsn(PUTFIELD, classType, "__controller", "Lrcl/internal/Entity;");
		} else {
			super.visitVarInsn(ALOAD, 0);
			super.visitMethodInsn(INVOKESTATIC, "rcl/internal/ThreadStagingArea", "getStagingArea", "()Lrcl/internal/ThreadStagingArea;");
			super.visitFieldInsn(PUTFIELD, classType, "__controller", "Lrcl/internal/Entity;");
		}
	}

	@Override
	public void visitFieldInsn(int opcode, String owner, String name, String desc
	) {
		if (opcode == PUTFIELD) {
			Type t = Type.getType(desc);

			if (t.getSort() == Type.OBJECT) {
				//TOS: v, t
				super.visitInsn(DUP2);
				//TOS: v, t, v, t
				super.visitMethodInsn(INVOKESTATIC, "rcl/RCL", "__assign", "(Ljava/lang/Object;Ljava/lang/Object;)V");
				//TOS: v, t
			}
		}

		if (!context.isFieldUnchecked(owner, name) && !this.name.equals("__isReadable") && !this.name.equals("__isWriteable")) {
			//We need to differentiate between reads and writes
			if (opcode == GETFIELD) {
				//TOS: t
				super.visitFieldInsn(GETSTATIC, classType, "$assertionsDisabled", "Z");
				//TOS: assertionsDisabled, t
				Label l0 = new Label();
				super.visitJumpInsn(IFNE, l0);
				//TOS: t
				super.visitInsn(DUP);
				//TOS: t, t
				super.visitTypeInsn(INSTANCEOF, "rcl/internal/Entity");
				//TOS: int, t
				super.visitJumpInsn(IFEQ, l0);
				//TOS: t
				super.visitInsn(DUP);
				//TOS: t, t
				super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isReadable", "()Z");
				//TOS: bool, t
				super.visitJumpInsn(IFNE, l0);
				//TOS: t
				super.visitTypeInsn(NEW, "java/lang/AssertionError");
				//TOS: error, t
				super.visitInsn(DUP);
				//TOS: error, error, t
				super.visitLdcInsn("Access denied to " + name);
				//TOS: message, error, error, t
				super.visitMethodInsn(INVOKESPECIAL, "java/lang/AssertionError", "<init>", "(Ljava/lang/Object;)V");
				//TOS: error, t
				super.visitInsn(ATHROW);
				//TOS: t
				super.visitLabel(l0);
				//TOS: t
			} else if (opcode == PUTFIELD) {
				Type t = Type.getType(desc);
				//Long and double need multiple values
				if (t.getSize() == 1) {
					super.visitFieldInsn(GETSTATIC, classType, "$assertionsDisabled", "Z");
					//TOS: assertionsDisabled, v, t
					Label l0 = new Label();
					Label l1 = new Label();
					super.visitJumpInsn(IFNE, l1);
					//TOS: v, t
					super.visitInsn(DUP2);
					//TOS: v, t, v, t
					super.visitInsn(POP);
					//TOS: t, v, t
					super.visitInsn(DUP);
					//TOS: t, t, v, t
					super.visitTypeInsn(INSTANCEOF, "rcl/internal/Entity");
					//TOS: int, t, v, t
					super.visitJumpInsn(IFEQ, l0);
					//TOS: t, v, t
					super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
					//TOS: int, v, t
					super.visitJumpInsn(IFNE, l1);
					//TOS: v, t
					super.visitTypeInsn(NEW, "java/lang/AssertionError");
					//TOS: error, v, t
					super.visitInsn(DUP);
					//TOS: error, error, v, t
					super.visitLdcInsn("Access denied to " + name);
					//TOS: name, error, error, v, t
					super.visitMethodInsn(INVOKESPECIAL, "java/lang/AssertionError", "<init>", "(Ljava/lang/Object;)V");
					//TOS: error, v, t
					super.visitInsn(ATHROW);
					//TOS: v, t
					super.visitJumpInsn(GOTO, l1);
					super.visitLabel(l0);
					//TOS: t, v, t
					super.visitInsn(POP);
					//TOS: v, t
					super.visitLabel(l1);
					//TOS: v, t
				} else if (t.getSize() == 2) {
					//Long or double
					super.visitFieldInsn(GETSTATIC, classType, "$assertionsDisabled", "Z");
					//TOS: assertionsDisabled, v, t
					Label l0 = new Label();
					Label l1 = new Label();
					Label l2 = new Label();
					super.visitJumpInsn(IFNE, l2);
					//TOS: v1, v2, object
					super.visitInsn(DUP2_X1);
					//TOS: v1, v2, object, v1, v2
					super.visitInsn(POP2);
					//TOS: object, v1, v2
					super.visitInsn(DUP);
					//TOS: object, object, v1, v2
					super.visitInsn(DUP);
					//TOS: object, object, object, v1, v2
					super.visitTypeInsn(INSTANCEOF, "rcl/internal/Entity");
					//TOS: int, object, object, v1, v2
					super.visitJumpInsn(IFEQ, l0);
					//TOS: object, object, v1, v2
					super.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
					//TOS: int, object, v1, v2
					super.visitJumpInsn(IFNE, l1);
					//TOS: object, v1, v2
					super.visitTypeInsn(NEW, "java/lang/AssertionError");
					//TOS: error, object, v1, v2
					super.visitInsn(DUP);
					//TOS: error, error, object, v1, v2
					super.visitLdcInsn("Access denied to " + name);
					//TOS: name, error, error, object, v1, v2
					super.visitMethodInsn(INVOKESPECIAL, "java/lang/AssertionError", "<init>", "(Ljava/lang/Object;)V");
					//TOS: error, object, v1, v2
					super.visitInsn(ATHROW);
					//TOS: object, v1, v2
					super.visitJumpInsn(GOTO, l1);
					super.visitLabel(l0);
					//TOS: object, object, v1, v2
					super.visitInsn(POP);
					//TOS: object, v1, v2
					super.visitLabel(l1);
					//TOS: object, v1, v2
					super.visitInsn(DUP_X2);
					//TOS: object, v1, v2, object
					super.visitInsn(POP);
					//TOS: v1, v2, object
					super.visitLabel(l2);
					//TOS: v1, v2, object
				}
			}
		}
		//Finally, put in the field instruction
		super.visitFieldInsn(opcode, owner, name, desc);
	}

}
