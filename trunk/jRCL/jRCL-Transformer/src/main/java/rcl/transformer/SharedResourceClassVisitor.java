/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.util.Arrays;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.*;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SharedResourceClassVisitor extends CheckedClassVisitor {

	private String name;

	public SharedResourceClassVisitor(ClassVisitor cv, TransformationContext context) {
		super(cv, context);
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.name = name;
		if (context.getClassification(superName) == ClassClassification.UNCHECKED) {
			interfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
			interfaces[interfaces.length - 1] = "rcl/internal/SharedResource";
		}

		super.visit(version, access, name, signature, superName, interfaces);
	}

	protected void generateRCLMethods() {

		//Add controller field
		{
			FieldVisitor fv = cv.visitField(ACC_PRIVATE + ACC_FINAL, "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;", "Ljava/util/concurrent/ConcurrentLinkedQueue<Lrcl/internal/Entity;>;", null);
			fv.visitEnd();
		}

		MethodVisitor mv;
		{
			mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
			mv.visitCode();
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 1);
			mv.visitEnd();
		}
		{
			mv = cv.visitMethod(ACC_PUBLIC + ACC_FINAL, "__share", "(Lrcl/internal/Entity;)V", null, null);
// ATTRIBUTE org.netbeans.SourceLevelAnnotations
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, "rcl/internal/DefaultSharedResource", "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "add", "(Ljava/lang/Object;)Z");
			mv.visitInsn(POP);
			mv.visitInsn(RETURN);
			mv.visitMaxs(2, 2);
			mv.visitEnd();
		}
		{
			mv = cv.visitMethod(ACC_PUBLIC + ACC_FINAL, "__release", "()V", null, null);
// ATTRIBUTE org.netbeans.SourceLevelAnnotations
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, "rcl/internal/DefaultSharedResource", "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "iterator", "()Ljava/util/Iterator;");
			mv.visitVarInsn(ASTORE, 1);
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/util/Iterator"}, 0, null);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z");
			Label l1 = new Label();
			mv.visitJumpInsn(IFEQ, l1);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;");
			mv.visitTypeInsn(CHECKCAST, "rcl/internal/Entity");
			mv.visitVarInsn(ASTORE, 2);
			mv.visitVarInsn(ALOAD, 2);
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
			Label l2 = new Label();
			mv.visitJumpInsn(IFEQ, l2);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, "rcl/internal/DefaultSharedResource", "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
			mv.visitVarInsn(ALOAD, 2);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "remove", "(Ljava/lang/Object;)Z");
			mv.visitInsn(POP);
			mv.visitLabel(l2);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitJumpInsn(GOTO, l0);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
			mv.visitInsn(RETURN);
			mv.visitMaxs(2, 3);
			mv.visitEnd();
		}
		{
			mv = cv.visitMethod(ACC_PUBLIC + ACC_FINAL, "__isWriteable", "()Z", null, null);
// ATTRIBUTE org.netbeans.SourceLevelAnnotations
			mv.visitCode();
			mv.visitInsn(ICONST_0);
			mv.visitVarInsn(ISTORE, 1);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, "rcl/internal/DefaultSharedResource", "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "iterator", "()Ljava/util/Iterator;");
			mv.visitVarInsn(ASTORE, 2);
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_APPEND, 2, new Object[]{Opcodes.INTEGER, "java/util/Iterator"}, 0, null);
			mv.visitVarInsn(ALOAD, 2);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z");
			Label l1 = new Label();
			mv.visitJumpInsn(IFEQ, l1);
			mv.visitVarInsn(ALOAD, 2);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;");
			mv.visitTypeInsn(CHECKCAST, "rcl/internal/Entity");
			mv.visitVarInsn(ASTORE, 3);
			mv.visitVarInsn(ALOAD, 3);
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
			Label l2 = new Label();
			mv.visitJumpInsn(IFEQ, l2);
			mv.visitInsn(ICONST_1);
			mv.visitVarInsn(ISTORE, 1);
			Label l3 = new Label();
			mv.visitJumpInsn(GOTO, l3);
			mv.visitLabel(l2);
			mv.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"rcl/internal/Entity"}, 0, null);
			mv.visitInsn(ICONST_0);
			mv.visitVarInsn(ISTORE, 1);
			mv.visitJumpInsn(GOTO, l1);
			mv.visitLabel(l3);
			mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
			mv.visitJumpInsn(GOTO, l0);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
			mv.visitVarInsn(ILOAD, 1);
			mv.visitInsn(IRETURN);
			mv.visitMaxs(1, 4);
			mv.visitEnd();
		}
		{
			mv = cv.visitMethod(ACC_PUBLIC + ACC_FINAL, "__isReadable", "()Z", null, null);
// ATTRIBUTE org.netbeans.SourceLevelAnnotations
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, "rcl/internal/DefaultSharedResource", "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "iterator", "()Ljava/util/Iterator;");
			mv.visitVarInsn(ASTORE, 1);
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/util/Iterator"}, 0, null);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z");
			Label l1 = new Label();
			mv.visitJumpInsn(IFEQ, l1);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;");
			mv.visitTypeInsn(CHECKCAST, "rcl/internal/Entity");
			mv.visitVarInsn(ASTORE, 2);
			mv.visitVarInsn(ALOAD, 2);
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isReadable", "()Z");
			Label l2 = new Label();
			mv.visitJumpInsn(IFEQ, l2);
			mv.visitInsn(ICONST_1);
			mv.visitInsn(IRETURN);
			mv.visitLabel(l2);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitJumpInsn(GOTO, l0);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
			mv.visitInsn(ICONST_0);
			mv.visitInsn(IRETURN);
			mv.visitMaxs(1, 3);
			mv.visitEnd();
		}
	}

	@Override
	protected CheckedMethodVisitor createMethodVisitor(MethodVisitor mv, int access, String name, String desc, String classType, TransformationContext context) {
		return new SharedResourceMethodVisitor(mv, access, name, desc, classType, context, superName);
	}
}
