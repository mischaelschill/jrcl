/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import org.objectweb.asm.MethodVisitor;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SharedResourceMethodVisitor extends CheckedMethodVisitor {

	public SharedResourceMethodVisitor(MethodVisitor mv, int access, String name, String desc, String classType, TransformationContext context, String superClassName) {
		super(mv, access, name, desc, classType, context, superClassName);
	}

	@Override
	protected void generateControllerInitializationCode() {
		mv.visitVarInsn(ALOAD, 0);
		mv.visitTypeInsn(NEW, "java/util/concurrent/ConcurrentLinkedQueue");
		mv.visitInsn(DUP);
		mv.visitMethodInsn(INVOKESPECIAL, "java/util/concurrent/ConcurrentLinkedQueue", "<init>", "()V");
		mv.visitFieldInsn(PUTFIELD, classType, "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, classType, "__controller", "Ljava/util/concurrent/ConcurrentLinkedQueue;");
//		mv.visitVarInsn(ALOAD, callerArgument);
		super.visitMethodInsn(INVOKESTATIC, "rcl/internal/ThreadStagingArea", "getStagingArea", "()Lrcl/internal/ThreadStagingArea;");
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/ConcurrentLinkedQueue", "add", "(Ljava/lang/Object;)Z");
		mv.visitInsn(POP);
		mv.visitInsn(RETURN);
	}

}
