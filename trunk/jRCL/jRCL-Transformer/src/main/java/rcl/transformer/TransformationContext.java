/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.io.IOException;
import rcl.annotations.AbstractProcess;
import rcl.annotations.Checked;
import rcl.annotations.Shared;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ASM4;
import org.objectweb.asm.Type;
import rcl.annotations.Unchecked;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class TransformationContext {

	private final HashMap<String, ClassClassification> classifications = new HashMap<>();
	private final HashSet<String> uncheckedFields = new HashSet<>();
	private final List<Pattern> checkedClasses = new ArrayList<>();
	private BytecodeLoader loader;

	{
		classifications.put("java/lang/Object", ClassClassification.UNCHECKED);
		classifications.put("java/lang/Thread", ClassClassification.UNCHECKED_PROCESS);
	}

	public TransformationContext() {
		loader = new ClasspathBytecodeLoader();
	}

	public TransformationContext(BytecodeLoader loader) {
		this.loader = loader;
	}

	public void setLoader(BytecodeLoader loader) {
		this.loader = loader;
	}
	
	public void addCheckedClassPattern (Pattern pattern) {
		checkedClasses.add(pattern);
	}
	
	public boolean isClassChecked(String internalName) {
		for (Pattern p : checkedClasses) {
			if (p.matcher(internalName).matches())
				return true;
		}
		return false;
	}	
	
	public ClassClassification getClassification(String type) {
		if (!classifications.containsKey(type)) {
			classify(type);
		}
		return classifications.get(type);
	}

	public boolean isFieldUnchecked(String owner, String name) {
		return uncheckedFields.contains(owner + "." + name);
	}

	public void markFieldUnchecked(String owner, String name) {
		uncheckedFields.add(owner + "." + name);
	}

	private void classify(String type) {
		ClassReader reader = loader.getBytecode(type);
		Classificator visitor = new Classificator(this);
		reader.accept(visitor, 0);
		classifications.put(type, visitor.getClassification());
	}

	public ClassClassification classify(byte[] bytecode) {
		ClassReader reader = new ClassReader(bytecode);
		Classificator visitor = new Classificator(this);
		reader.accept(visitor, 0);
		classifications.put(visitor.getName(), visitor.getClassification());
		return visitor.getClassification();
	}

	public ClassClassification classify(InputStream bytecode) {
		try {
			ClassReader reader = new ClassReader(bytecode);
			Classificator visitor = new Classificator(this);
			reader.accept(visitor, 0);
			classifications.put(visitor.getName(), visitor.getClassification());
			return visitor.getClassification();
		} catch (IOException ex) {
			Logger.getLogger(TransformationContext.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}
}

class Classificator extends ClassVisitor implements Opcodes {

	private ClassClassification classification = ClassClassification.UNCHECKED;
	private ClassClassification superClassification;
	private final TransformationContext context;
	private String name;
	private String superName;
	private boolean isThread;

	public Classificator(TransformationContext context) {
		super(ASM4);
		this.context = context;
	}

	@Override
	public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
		Type t = Type.getType(desc);
		if (t.equals(Type.getType(Checked.class))) {
			if (classification == ClassClassification.UNCHECKED_PROCESS ||
						classification == ClassClassification.PROCESS) {
				classification = ClassClassification.PROCESS;
			} else {
				assert classification == ClassClassification.UNCHECKED ||
						classification == ClassClassification.RESOURCE;
				classification = ClassClassification.RESOURCE;
			}
		} else if (t.equals(Type.getType(Shared.class))) {
			assert superClassification == ClassClassification.UNCHECKED ||
						classification == ClassClassification.SHARED_RESOURCE;
			classification = ClassClassification.SHARED_RESOURCE;
		} else if (t.equals(Type.getType(AbstractProcess.class))) {
			assert superClassification == ClassClassification.UNCHECKED ||
						classification == ClassClassification.ABSTRACT_PROCESS;
			classification = ClassClassification.ABSTRACT_PROCESS;
		} else if (t.equals(Type.getType(Unchecked.class))) {
			assert superClassification == ClassClassification.UNCHECKED;
			classification = ClassClassification.UNCHECKED;
		}
		return super.visitAnnotation(desc, visible);
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.superName = superName;
		this.name = name;
		this.superClassification = context.getClassification(superName);
		if ((access & ACC_INTERFACE) > 0) {
			classification = ClassClassification.INTERFACE;
		} else {
			classification = context.getClassification(superName);
		}
		if (classification == ClassClassification.UNCHECKED && context.isClassChecked(name)) {
			classification = ClassClassification.RESOURCE;
		} else if (classification == ClassClassification.UNCHECKED_PROCESS && context.isClassChecked(name)) {
			classification = ClassClassification.PROCESS;
		}
		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public void visitEnd() {
		if (classification == ClassClassification.UNCHECKED) {
			classification = context.getClassification(superName);
		}
		super.visitEnd();
	}

	public ClassClassification getClassification() {
		return classification;
	}

	public String getName() {
		return name;
	}

	public boolean isThread() {
		return isThread;
	}
}
