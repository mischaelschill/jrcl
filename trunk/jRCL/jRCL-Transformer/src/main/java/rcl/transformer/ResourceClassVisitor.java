/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.util.Arrays;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.*;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class ResourceClassVisitor extends CheckedClassVisitor {

	private String name;

	public ResourceClassVisitor(ClassVisitor cv, TransformationContext context) {
		super(cv, context);
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.name = name;
		if (context.getClassification(superName) == ClassClassification.UNCHECKED) {
			interfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
			interfaces[interfaces.length - 1] = "rcl/internal/Resource";
		}

		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	protected void generateRCLMethods() {
		//Add controller field
		{
			FieldVisitor fv = super.visitField(ACC_PRIVATE + ACC_SYNTHETIC, "__controller", "Lrcl/internal/Entity;", null, null);
			fv.visitEnd();
		}

		//Add __getController
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC + ACC_FINAL, "__getController", "()Lrcl/internal/Entity;", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, name, "__controller", "Lrcl/internal/Entity;");
			mv.visitInsn(ARETURN);
			mv.visitMaxs(1, 1);
			mv.visitEnd();
		}

		//Add __pass
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC + ACC_FINAL, "__pass", "(Lrcl/internal/Entity;)V", null, null);
			mv.visitCode();
			mv.visitFieldInsn(GETSTATIC, name, "$assertionsDisabled", "Z");
			Label l0 = new Label();
			mv.visitJumpInsn(IFNE, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, name, "__isWriteable", "()Z");
			mv.visitJumpInsn(IFNE, l0);
			mv.visitTypeInsn(NEW, "java/lang/AssertionError");
			mv.visitInsn(DUP);
			mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
			mv.visitInsn(DUP);
			mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V");
			mv.visitLdcInsn("Control violation while passing object \"");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;");
			mv.visitLdcInsn("\" to \"");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;");
			mv.visitLdcInsn("\"");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
			mv.visitMethodInsn(INVOKESPECIAL, "java/lang/AssertionError", "<init>", "(Ljava/lang/Object;)V");
			mv.visitInsn(ATHROW);
			mv.visitLabel(l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitFieldInsn(PUTFIELD, name, "__controller", "Lrcl/internal/Entity;");
			mv.visitInsn(RETURN);
			mv.visitMaxs(4, 2);
			mv.visitEnd();
		}

		//Add __isWriteable
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC + ACC_FINAL, "__isWriteable", "()Z", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, name, "__controller", "Lrcl/internal/Entity;");
//			mv.visitTypeInsn(CHECKCAST, "rcl/internal/Entity");
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isWriteable", "()Z");
//			Label l0 = new Label();
//			mv.visitJumpInsn(IFNE, l0);
//			mv.visitVarInsn(ALOAD, 0);
//			mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "holdsLock", "(Ljava/lang/Object;)Z");
//			Label l1 = new Label();
//			mv.visitJumpInsn(IFEQ, l1);
//			mv.visitLabel(l0);
//			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//			mv.visitInsn(ICONST_1);
//			Label l2 = new Label();
//			mv.visitJumpInsn(GOTO, l2);
//			mv.visitLabel(l1);
//			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//			mv.visitInsn(ICONST_0);
//			mv.visitLabel(l2);
//			mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			mv.visitInsn(IRETURN);
			mv.visitMaxs(1, 1);
			mv.visitEnd();
		}

		//Add __isReadable
		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC + ACC_FINAL, "__isReadable", "()Z", null, null);
			// ATTRIBUTE org.netbeans.SourceLevelAnnotations
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, name, "__controller", "Lrcl/internal/Entity;");
//			mv.visitTypeInsn(CHECKCAST, "rcl/internal/Entity");
			mv.visitMethodInsn(INVOKEINTERFACE, "rcl/internal/Entity", "__isReadable", "()Z");
//			Label l0 = new Label();
//			mv.visitJumpInsn(IFNE, l0);
//			mv.visitVarInsn(ALOAD, 0);
//			mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "holdsLock", "(Ljava/lang/Object;)Z");
//			Label l1 = new Label();
//			mv.visitJumpInsn(IFEQ, l1);
//			mv.visitLabel(l0);
//			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//			mv.visitInsn(ICONST_1);
//			Label l2 = new Label();
//			mv.visitJumpInsn(GOTO, l2);
//			mv.visitLabel(l1);
//			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//			mv.visitInsn(ICONST_0);
//			mv.visitLabel(l2);
//			mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			mv.visitInsn(IRETURN);
			mv.visitMaxs(1, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_SYNTHETIC + ACC_FINAL, "__setControl", "(Lrcl/internal/Entity;)Lrcl/internal/Entity;", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, type, "__controller", "Lrcl/internal/Entity;");
			mv.visitVarInsn(ASTORE, 2);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitFieldInsn(PUTFIELD, type, "__controller", "Lrcl/internal/Entity;");
			mv.visitVarInsn(ALOAD, 2);
			mv.visitInsn(ARETURN);
			mv.visitMaxs(2, 3);
			mv.visitEnd();
		}
	}
}
