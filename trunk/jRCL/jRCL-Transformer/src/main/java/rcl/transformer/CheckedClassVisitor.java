/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import rcl.annotations.Unchecked;
import java.util.Arrays;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ACC_FINAL;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SYNTHETIC;
import static org.objectweb.asm.Opcodes.ASM4;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.ICONST_0;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.PUTSTATIC;
import static org.objectweb.asm.Opcodes.RETURN;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public abstract class CheckedClassVisitor extends ClassVisitor implements Opcodes {

	protected String type;

	protected String superName;

	protected final TransformationContext context;

	public CheckedClassVisitor(ClassVisitor cv, TransformationContext context) {
		super(ASM4, cv);
		this.context = context;
	}

	private boolean hasAssertion = false;
	private boolean hasAssertionInitialized = false;

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.type = name;
		this.superName = superName;
		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public FieldVisitor visitField(int access, final String name, String desc, String signature, Object value) {
		if (name.equals("$assertionsDisabled")) {
			hasAssertion = true;
			return super.visitField(access, name, desc, signature, value);
		}
		if ((access & ACC_STATIC) != 0) {
			return super.visitField(access, name, desc, signature, value);
		}
		if ((access & ACC_FINAL) != 0) {
			context.markFieldUnchecked(type, name);
			return super.visitField(access, name, desc, signature, value);
		}
		return new FieldVisitor(api, super.visitField(access, name, desc, signature, value)) {
			@Override
			public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
				if (Type.getType(desc).equals(Type.getType(Unchecked.class))) {
					context.markFieldUnchecked(type, name);
				}
				return super.visitAnnotation(desc, visible); //To change body of generated methods, choose Tools | Templates.
			}
		};
	}

	@Override
	public void visitEnd() {
		if (!hasAssertion) {
			FieldVisitor fv = visitField(ACC_FINAL + ACC_STATIC + ACC_SYNTHETIC, "$assertionsDisabled", "Z", null, null);
			fv.visitEnd();
		}
		if (!hasAssertionInitialized) {
			MethodVisitor mv;
			mv = visitMethod(ACC_STATIC + ACC_SYNTHETIC, "<clinit>", "()V", null, null);
			mv.visitCode();
			mv.visitLdcInsn(Type.getType("L" + type + ";"));
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "desiredAssertionStatus", "()Z");
			Label l0 = new Label();
			mv.visitJumpInsn(IFNE, l0);
			mv.visitInsn(ICONST_1);
			Label l1 = new Label();
			mv.visitJumpInsn(GOTO, l1);
			mv.visitLabel(l0);
			mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			mv.visitInsn(ICONST_0);
			mv.visitLabel(l1);
			mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			mv.visitFieldInsn(PUTSTATIC, type, "$assertionsDisabled", "Z");
			mv.visitInsn(RETURN);
			mv.visitMaxs(1, 0);
			mv.visitEnd();
		}
		ClassClassification superClassification = context.getClassification(superName);
		if (superClassification == ClassClassification.UNCHECKED || 
				superClassification == ClassClassification.UNCHECKED_PROCESS) {
			generateRCLMethods();
		}
		super.visitEnd();
	}

	protected abstract void generateRCLMethods();

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
//		if (name.equals("<init>")) {
//
//			Type[] arguments = Type.getArgumentTypes(desc);
//			arguments = Arrays.copyOf(arguments, arguments.length + 1);
//			arguments[arguments.length - 1] = Type.getObjectType("rcl/internal/Entity");
//			Type newType = Type.getMethodType(Type.VOID_TYPE, arguments);
//			{
//				MethodVisitor mv;
//				mv = super.visitMethod(access | ACC_SYNTHETIC, name, desc, signature, exceptions);
//				mv.visitCode();
//				int i = 0;
//				mv.visitVarInsn(ALOAD, i++);
//				for (Type t : Type.getArgumentTypes(desc)) {
//					mv.visitVarInsn(t.getOpcode(ILOAD), i);
//					i += t.getSize();
//				}
//				mv.visitMethodInsn(INVOKESTATIC, "rcl/internal/FakeProcess", "getFakeProcess", "()Lrcl/internal/FakeProcess;");
//				mv.visitMethodInsn(INVOKESPECIAL, this.type, "<init>", newType.getDescriptor());
//				mv.visitInsn(RETURN);
//				mv.visitMaxs(i + 1, i);
//				mv.visitEnd();
//			}
//			CheckedMethodVisitor ccv = createMethodVisitor(super.visitMethod(access, name,
//					newType.getDescriptor(), signature, exceptions), access, name,
//					newType.getDescriptor(), this.type, context);
//			LocalVariablesSorter sort = new LocalVariablesSorter(access, newType.getDescriptor(), ccv);
//			ccv.setSorter(sort);
//			return sort;
//		} else 
			
		if (name.equals("<clinit>")) {
			hasAssertionInitialized = true;
			return new AssertionInitializationMethodVisitor(
					super.visitMethod(access, name, desc, signature, exceptions), this.type);
		}
		
		CheckedMethodVisitor ccv = createMethodVisitor(super.visitMethod(access, name,
				desc, signature, exceptions), access, name,
				desc, this.type, context);
		LocalVariablesSorter sort = new LocalVariablesSorter(access, desc, ccv);
		ccv.setSorter(sort);
		
		return sort;
	}
	
	protected CheckedMethodVisitor createMethodVisitor (MethodVisitor mv, int access, String name, String desc, String classType, TransformationContext context) {
		return new CheckedMethodVisitor(mv, access, name, desc, classType, context, superName);
	}
}
