/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rcl.transformer;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ASM4;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.ICONST_0;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.PUTSTATIC;
import static org.objectweb.asm.Opcodes.RETURN;
import org.objectweb.asm.Type;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class AssertionInitializationMethodVisitor extends MethodVisitor implements Opcodes {

	private boolean hasAssertions = false;
	private final String className;

	public AssertionInitializationMethodVisitor(MethodVisitor mv, String className) {
		super(ASM4, mv);
		this.className = className;
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String desc) {
		if (opcode == INVOKEVIRTUAL && owner.equals("java/lang/Class") && name.equals("desiredAssertionStatus")) {
			hasAssertions = true;
		}
		super.visitMethodInsn(opcode, owner, name, desc); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void visitInsn(int opcode) {
		if (opcode == RETURN && hasAssertions == false) {
			super.visitLdcInsn(Type.getType("L" + className + ";"));
			super.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "desiredAssertionStatus", "()Z");
			Label l0 = new Label();
			super.visitJumpInsn(IFNE, l0);
			super.visitInsn(ICONST_1);
			Label l1 = new Label();
			super.visitJumpInsn(GOTO, l1);
			super.visitLabel(l0);
//			super.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
			super.visitInsn(ICONST_0);
			super.visitLabel(l1);
//			super.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{Opcodes.INTEGER});
			super.visitFieldInsn(PUTSTATIC, className, "$assertionsDisabled", "Z");
		}
		super.visitInsn(opcode); //To change body of generated methods, choose Tools | Templates.
	}
}
