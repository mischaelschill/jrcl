package rcl.transformer;

import java.lang.instrument.Instrumentation;
import java.util.regex.Pattern;
import rcl.transformer.TransformationContext;

/**
 *
 * @author mischael
 */
public class RCLPreMain {

	public static void premain(String agentArgs, Instrumentation inst) {
		TransformationContext context = new TransformationContext();
		String[] patterns = agentArgs.split(",");
		int i = 0;
		for (String p: patterns) {
			p = p.replace(".", "/");
			p = p.replace("*", ".*");
			p = p.replace("?", ".?");

			context.addCheckedClassPattern(Pattern.compile(p));
		}
		
		inst.addTransformer(new Transformer(context));
	}	
}
