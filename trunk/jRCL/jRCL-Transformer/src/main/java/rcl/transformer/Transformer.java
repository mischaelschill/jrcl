/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.io.PrintWriter;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

/**
 * @author mschill
 */
public class Transformer implements ClassFileTransformer {

	public final TransformationContext context;

	public Transformer(TransformationContext context) {
		this.context = context;
	}

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			ClassClassification classification = context.classify(classfileBuffer);
			ClassReader cr = new ClassReader(classfileBuffer);
			ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
			ClassVisitor cv = cw;
			//cv = new TraceClassVisitor(cv, new PrintWriter(System.err));
			cv = new CheckClassAdapter(cv, false);
			switch (classification) {
				case RESOURCE:
					// Transform checked classes
					cv = new ResourceClassVisitor(cv, context);
					break;
				case SHARED_RESOURCE:
					// Transform shared resource classes
					cv = new SharedResourceClassVisitor(cv, context);
					break;
				case PROCESS:
					// Transform processes (i.e. classes inheriting from java.lang.Thread)
					cv = new ProcessClassVisitor(cv, context);
					break;
				case ABSTRACT_PROCESS:
					// Transform abstract processes (e. g. locks)
					cv = new AbstractProcessClassVisitor(cv, context);
					break;
				default:
					return classfileBuffer;
			}
			cr.accept(cv, ClassReader.EXPAND_FRAMES);

//			ClassReader cwa = new ClassReader(cw.toByteArray());
//			cv = new TraceClassVisitor(new PrintWriter(System.err));
//			cv = new CheckClassAdapter(cv);
//			cwa.accept(cv, 0);
			return cw.toByteArray();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
