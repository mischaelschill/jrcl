/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.util.Arrays;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class AbstractProcessClassVisitor extends CheckedClassVisitor {

	private String name;

	public AbstractProcessClassVisitor(ClassVisitor cv, TransformationContext context) {
		super(cv, context);
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.name = name;
		if (context.getClassification(superName) == ClassClassification.UNCHECKED) {
			interfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
			interfaces[interfaces.length - 1] = "rcl/internal/AbstractProcess";
		}

		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	protected void generateRCLMethods() {
	}

	@Override
	protected CheckedMethodVisitor createMethodVisitor(MethodVisitor mv, int access, String name, String desc, String classType, TransformationContext context) {
		return new AbstractProcessMethodVisitor(mv, access, name, desc, classType, context, superName);
	}
}
