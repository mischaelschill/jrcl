/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rcl.transformer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;

/**
 * Applies the bytecode transformations needed for RCL support.
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class AOTTransformer {

	public static void main(String[] args) throws Exception {
		final TransformationContext context = new TransformationContext();
		Path classes = null;
		List<Path> libraries = new ArrayList<>();

		for (int argi = 0; argi < args.length; argi++) {
			String s = args[argi];
			if (s.startsWith("--")) {
				//Is a long option
				s = s.substring(2);
				if (s.startsWith("checked=")) {
					s = s.substring("checked=".length());
					if (s.isEmpty()) {
						System.err.println("Missing list of patterns as option to --checked=");
						System.exit(1);
					}
					parseCheckedClassExpressions(s, context);
				} else if (s.startsWith("lib=")) {
					s = s.substring("lib=".length());
					if (s.isEmpty()) {
						System.err.println("Missing files or directories as option to --lib=");
						System.exit(1);
					}
					for (String l : s.split(",")) {
						Path p = FileSystems.getDefault().getPath(l);
						if (l.endsWith(".jar")) {
							p = FileSystems.newFileSystem(p, null).getPath("/");
						}
						libraries.add(p);
					}
				} else {
					System.err.println("Unknown option --" + s);
					System.exit(1);
				}
			} else if (s.startsWith("-")) {
				if (s.startsWith("c")) {
					s = s.substring(1);
					if (s.isEmpty()) {
						if (argi < args.length) {
							argi++;
							s = args[argi];
						} else {
							System.err.println("Missing list of patterns as option to -c");
							System.exit(1);
						}
					}
					parseCheckedClassExpressions(s, context);
				} else if (s.startsWith("l")) {
					s = s.substring(1);
					if (s.isEmpty()) {
						if (argi < args.length) {
							argi++;
							s = args[argi];
						} else {
							System.err.println("Missing list of files or directories as option to -l");
							System.exit(1);
						}
					}
					for (String l : s.split(",")) {
						Path p = FileSystems.getDefault().getPath(l);
						if (l.endsWith(".jar")) {
							p = FileSystems.newFileSystem(p, null).getPath("/");
						}
						libraries.add(p);
					}
				} else {
					System.err.println("Unknown option -" + s);
					System.exit(1);
				}
			} else {
				if (classes == null) {
					Path p = FileSystems.getDefault().getPath(s);
					if (s.endsWith(".jar")) {
						p = FileSystems.newFileSystem(p, null).getPath("/");
					}
					classes = p;
				} else {
					System.err.println("I do not know what to do with " + s);
					System.exit(1);
				}
			}
		}

		if (classes == null) {
			classes = FileSystems.getDefault().getPath(".");
		}
		
		libraries.add(classes);
		context.setLoader(new ExternalBytecodeLoader(libraries));

		Files.walkFileTree(classes, new FileVisitor<Path>(){

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (file.getFileName().toString().endsWith(".class")) {
					InputStream is = Files.newInputStream(file);
					byte[] buffer = new byte[(int)Math.min(Files.size(file), Integer.MAX_VALUE)];
					int len = is.read(buffer);
					if (len < 0 || len == Integer.MAX_VALUE || len != buffer.length) {
						System.err.println ("Illegal class file encountered: " + file);
						System.exit(1);
					}
					ClassClassification classification = context.classify(buffer);
					ClassReader cr = new ClassReader(buffer);
					ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
					ClassVisitor cv = cw;
					//cv = new TraceClassVisitor(cv, new PrintWriter(System.err));
					cv = new CheckClassAdapter(cv, false);
					switch (classification) {
						case RESOURCE:
							// Transform checked classes
							cv = new ResourceClassVisitor(cv, context);
							break;
						case SHARED_RESOURCE:
							// Transform shared resource classes
							cv = new SharedResourceClassVisitor(cv, context);
							break;
						case PROCESS:
							// Transform processes (i.e. classes inheriting from java.lang.Thread)
							cv = new ProcessClassVisitor(cv, context);
							break;
						case ABSTRACT_PROCESS:
							// Transform abstract processes (e. g. locks)
							cv = new AbstractProcessClassVisitor(cv, context);
							break;
						default:
							return FileVisitResult.CONTINUE;
					}
					cr.accept(cv, 0);
					OutputStream o = Files.newOutputStream(file, StandardOpenOption.WRITE);
					o.write(cw.toByteArray());
					o.close();
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private static void parseCheckedClassExpressions(String s, TransformationContext context) {
		String[] patterns = s.split(",");
		for (String p : patterns) {
			p = p.replace(".", "/");
			p = p.replace("*", ".*");
			p = p.replace("?", ".?");

			context.addCheckedClassPattern(Pattern.compile(p));
		}
	}
}

class ExternalBytecodeLoader implements BytecodeLoader {

	private final List<Path> sources = new ArrayList<>();
	
	public ExternalBytecodeLoader(Collection<Path> sources) {
		this.sources.addAll(sources);
	}

	public void addSource(Path source) {
		sources.add(source);
	}

	@Override
	public ClassReader getBytecode(String internalName) {
		String classFileName = internalName.replace('.', '/') + ".class";
		try {
			for (Path p : sources) {
				if (Files.isDirectory(p)) {
					p = p.resolve(classFileName);
					if (Files.exists(p)) {
						return new ClassReader(Files.newInputStream(p));
					}
				} else {
					JarFile jar = new JarFile(p.toFile());
					ZipEntry z = jar.getEntry(classFileName);
					if (z != null) {
						return new ClassReader(jar.getInputStream(z));
					}
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(ExternalBytecodeLoader.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

}

class ClasspathBytecodeLoader implements BytecodeLoader {

	@Override
	public ClassReader getBytecode(String internalName) {
		try {
			return new ClassReader(ClassLoader.getSystemResourceAsStream(internalName.replace('.', '/') + ".class"));
		} catch (IOException ex) {
			Logger.getLogger(ExternalBytecodeLoader.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

}
