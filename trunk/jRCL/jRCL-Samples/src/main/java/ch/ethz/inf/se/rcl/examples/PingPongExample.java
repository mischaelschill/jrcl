package ch.ethz.inf.se.rcl.examples;

import rcl.util.concurrent.LightLock;
import rcl.annotations.Checked;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mischael
 */
@Checked
public class PingPongExample extends Thread {

	private LightLock<Ball> ping, pong;

	private final int passes;

	private final boolean measureTime;

	public PingPongExample(int passes, boolean measureTime) {
		this.passes = passes;
		this.measureTime = measureTime;
	}

	public PingPongExample() {
		this.passes = 100;
		measureTime = false;
	}

	@Override
	public void run() {
		Ball b = new Ball();
		ping = new LightLock<>(b);
		ping.lock();
		pong = new LightLock<>(b);
		pong.lock();
		Player p1 = (new Player(ping, pong, passes));
		Player p2 = (new Player(pong, ping, passes));
		long start = System.currentTimeMillis();
		p1.start();
		p2.start();
		pong.unlock();
		try {
			p1.join();
		} catch (InterruptedException ex) {
			Logger.getLogger(PingPongExample.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			p2.join();
		} catch (InterruptedException ex) {
			Logger.getLogger(PingPongExample.class.getName()).log(Level.SEVERE, null, ex);
		}
		long stop = System.currentTimeMillis();
		if (measureTime) {
			System.out.println((stop - start));
		}
	}
}

@Checked
class Player extends Thread {

	final LightLock<Ball> ping, pong;
	final int passes;
	final Ball ball;

	public Player(LightLock<Ball> ping, LightLock<Ball> pong, int passes) {
		this.ping = ping;
		this.pong = pong;
		assert passes >= 0;
		this.passes = passes;
		this.ball = ping.getContent();
	}

	@Override
	public void run() {
		for (int i = 0; i < passes; i++) {
			ping.lock();
			ball.bounce();
			pong.unlock();
		}
	}
}

@Checked
class Ball {

	int bounces;

	public void bounce() {
		bounces++;
	}
}
