/*
 Lonestar Benchmark Suite for irregular applications that exhibit 
 amorphous data-parallelism.

 Center for Grid and Distributed Computing
 The University of Texas at Austin

 Copyright (C) 2007, 2008, 2009 The University of Texas at Austin

 Licensed under the Eclipse Public License, Version 1.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.eclipse.org/legal/epl-v10.html

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 File: Mesh.java 
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Edge;
import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Node;
import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.UndirectedGraph;
import java.io.FileInputStream;
import java.util.zip.GZIPInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import rcl.annotations.Checked;
import rcl.collections.SafeDeque;
import rcl.collections.SafeSet;

@Checked
public class Mesh extends UndirectedGraph<Element, Element.Edge> {

	protected final Map<Element.Edge, Node<Element, Element.Edge>> edgeMap = new HashMap<Element.Edge, Node<Element, Element.Edge>>();

	public Set<Node<Element, Element.Edge>> getBad() {
		Set<Node<Element, Element.Edge>> ret = new SafeSet<>(new HashSet<Node<Element, Element.Edge>>());
		for (Node<Element, Element.Edge> node : this) {
			node.getLock().lock();
			try {
				Element element = node.getData();
				if (element.isBad()) {
					ret.add(node);
				}
			} finally {
				node.getLock().unlock();
			}
		}
		return ret;
	}

	public boolean verify() {
		// ensure consistency of elements
		for (Node<Element, Element.Edge> node : this) {
			if (node.getLock().isLocked()) 
				System.err.println(node);
		}
		for (Node<Element, Element.Edge> node : this) {
			node.getLock().lock();
				Element element = node.getData();
				if (element.getDim() == 2) {
					if (node.getEdges().size() != 1) {
						System.out.println("-> Segment " + element + " has " + node.getEdges().size() + " relation(s)");
						return false;
					}
				} else if (element.getDim() == 3) {
					if (node.getEdges().size() != 3) {
						System.out.println("-> Triangle " + element + " has " + node.getEdges().size() + " relation(s)");
						return false;
					}
				} else {
					System.out.println("-> Figures with " + element.getDim() + " edges");
					return false;
				}
		}
		// ensure reachability
		Node<Element, Element.Edge> start = this.iterator().next();
		Deque<Node<Element, Element.Edge>> remaining = new SafeDeque<>(new ArrayDeque<Node<Element, Element.Edge>>());
		Set<Node<Element, Element.Edge>> found = new SafeSet<>(new HashSet<Node<Element, Element.Edge>>());
		remaining.push(start);
		while (!remaining.isEmpty()) {
			Node<Element, Element.Edge> node = remaining.pop();
			if (!found.contains(node)) {
				found.add(node);
				for (Node<Element, Element.Edge> neighbor : node.getNeighbors()) {
					remaining.push(neighbor);
				}
			}
		}
		if (found.size() != this.size()) {
			System.out.println("Not all elements are reachable");
			return false;
		}
		for (Node<Element, Element.Edge> node : this) 
			node.getLock().unlock();

		return true;
	}
	
	private static Scanner getScanner(String filename) throws IOException {
		try {
			return new Scanner(new GZIPInputStream(new FileInputStream(filename + ".gz")));
		} catch (FileNotFoundException ex) {
			return new Scanner(new FileInputStream(filename));
		}
	}

	private Tuple[] readNodes(String filename) throws IOException {
		Scanner scanner = getScanner(filename + ".node");

		int ntups = scanner.nextInt();
		scanner.nextInt();
		scanner.nextInt();
		scanner.nextInt();

		Tuple[] tuples = new Tuple[ntups];
		for (int i = 0; i < ntups; i++) {
			int index = scanner.nextInt();
			double x = scanner.nextDouble();
			double y = scanner.nextDouble();
			scanner.nextDouble(); // z
			tuples[index] = new Tuple(x, y, 0);
		}

		return tuples;
	}

	private void readElements(String filename, Tuple[] tuples) throws IOException {
		Scanner scanner = getScanner(filename + ".ele");

		int nels = scanner.nextInt();
		scanner.nextInt();
		scanner.nextInt();
		Element[] elements = new Element[nels];
		for (int i = 0; i < nels; i++) {
			int index = scanner.nextInt();
			int n1 = scanner.nextInt();
			int n2 = scanner.nextInt();
			int n3 = scanner.nextInt();
			elements[index] = new Element(tuples[n1], tuples[n2], tuples[n3]);
			addElement(elements[index]);
		}
	}

	private void readPoly(String filename, Tuple[] tuples) throws IOException {
		Scanner scanner = getScanner(filename + ".poly");

		scanner.nextInt();
		scanner.nextInt();
		scanner.nextInt();
		scanner.nextInt();
		int nsegs = scanner.nextInt();
		scanner.nextInt();
		Element[] segments = new Element[nsegs];
		for (int i = 0; i < nsegs; i++) {
			int index = scanner.nextInt();
			int n1 = scanner.nextInt();
			int n2 = scanner.nextInt();
			scanner.nextInt();
			segments[index] = new Element(tuples[n1], tuples[n2]);
			addElement(segments[index]);
		}
	}

  // .poly contains the perimeter of the mesh; edges basically, which is why it
	// contains pairs of nodes
	public void read(String basename) throws IOException {
		Tuple[] tuples = readNodes(basename);
		readElements(basename, tuples);
		readPoly(basename, tuples);
	}

	protected Node<Element, Element.Edge> addElement(Element element) {
		Node<Element, Element.Edge> node = new Node(element);
		add (node);
		for (int i = 0; i < element.numEdges(); i++) {
			Element.Edge edge = element.getEdge(i);
			if (!edgeMap.containsKey(edge)) {
				edgeMap.put(edge, node);
			} else {
				node.getLock().lock();
				Node<Element, Element.Edge> other = edgeMap.get(edge);
				other.getLock().lock();
				try {
					node.addEdge(other, edge);
				} finally {
					node.getLock().unlock();
					other.getLock().unlock();
				}
				edgeMap.remove(edge);
			}
		}
		return node;
	}
}
