/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Node;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import rcl.annotations.Checked;
import rcl.collections.SafeSet;
import rcl.util.concurrent.Queues;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class ParallelDelaunayRefinement {

	public void run(String inputFile, int numWorkers, boolean outputTimes) throws InterruptedException, IOException {
		Mesh mesh = new Mesh();
		mesh.read(inputFile);

		BlockingQueue<WorkItem> worklist = Queues.fromBlockingQueue(new LinkedBlockingQueue<WorkItem>());
		worklist.addAll(WorkItem.itemCollection(mesh.getBad()));

		final Counter finished = new Counter(worklist.size());

		long startTime = System.currentTimeMillis();
		Set<DelaunayRefiner> workers = new SafeSet<>(new HashSet<DelaunayRefiner>());
		for (int i = 0; i < numWorkers; i++) {
			workers.add(new DelaunayRefiner(finished, worklist, mesh, "Worker 8"));
		}

		for (DelaunayRefiner r : workers) {
			r.start();
		}

		finished.waitUntilFinished();
		long stopTime = System.currentTimeMillis();
		for (int i = 0; i < workers.size(); i++) {
			worklist.add(new WorkItem(null));
		}

		if (outputTimes) {
			System.out.println((stopTime - startTime));
		}

//		assert mesh.verify();

	}
}
