package ch.ethz.inf.se.rcl.examples.linkedlist;

import rcl.RCL;
import rcl.annotations.Checked;

/**
 *
 * @author mischael
 */
@Checked
public class LinkedList {
	protected LLNode first;
	
	public LinkedList splitBefore(int index) {
		LinkedList result = new LinkedList();
		result.first = first;
		RCL.pass(first, result);
		//Find split point
		LLNode cur = first;
		for (int i = 1; i < index; i++) {
			cur = cur.next;
			RCL.pass(cur, result);
		}
		first = cur.next;
		//Make the cut
		cur.next = null;
		return result;
	}
}

@Checked
class LLNode {
	LLNode next;
}