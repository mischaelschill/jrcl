/*
 Lonestar Benchmark Suite for irregular applications that exhibit 
 amorphous data-parallelism.

 Center for Grid and Distributed Computing
 The University of Texas at Austin

 Copyright (C) 2007, 2008, 2009 The University of Texas at Austin

 Licensed under the Eclipse Public License, Version 1.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.eclipse.org/legal/epl-v10.html

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 File: Cavity.java 
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Edge;
import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Node;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import rcl.annotations.Checked;
import rcl.collections.SafeDeque;
import rcl.collections.SafeList;
import rcl.collections.SafeSet;

@Checked
public class Cavity {
	
	protected Tuple center;
	protected Node<Element, Element.Edge> centerNode;
	protected Element centerElement;
	protected int dim;
	protected final Deque<Node<Element, Element.Edge>> frontier;
	protected final SubMesh pre; // the cavity itself
	protected final SubMesh post; // what the new elements should look like
	private final Mesh graph;
	protected final Set<Edge<Element.Edge>> connections;

	// the edge-relations that connect the boundary to the cavity
	public Cavity(Mesh mesh) {
		center = null;
		frontier = new SafeDeque<>(new ArrayDeque<Node<Element, Element.Edge>>());
		pre = new SubMesh();
		post = new SubMesh();
		graph = mesh;
		connections = new SafeSet<>(new HashSet<Edge<Element.Edge>>());
	}
	
	public SubMesh getPre() {
		return pre;
	}
	
	public SubMesh getPost() {
		return post;
	}
	
	public void triggerAbort() {
	}
	
	public void triggerBorderConflict() {
	}
	
	public boolean initialize(Node<Element, Element.Edge> node) {
		pre.reset();
		post.reset();
		connections.clear();
		frontier.clear();
		centerNode = node;
		if (!centerNode.getLock().tryLock()) {
			return false;
		}
		centerElement = centerNode.getData();
		boolean condition;
		synchronized (graph) {
			condition = graph.contains(centerNode) && centerElement.isObtuse();
		}
		while (condition) {
			Edge<Element.Edge> oppositeEdge = getOpposite(centerNode);
			if (oppositeEdge == null) {
				centerNode.getLock().unlock();
				return false;
			}
			boolean b = oppositeEdge.getNode1() == centerNode;
			centerNode.getLock().unlock();
			if (b) {
				centerNode = oppositeEdge.getNode2();
			} else {
				centerNode = oppositeEdge.getNode1();
			}
			
			if (!centerNode.getLock().tryLock()) {
				return false;
			}
			centerElement = centerNode.getData();
			assert centerNode != null;
			synchronized (graph) {
				condition = graph.contains(centerNode) && centerElement.isObtuse();
			}
		}
		center = centerElement.center();
		dim = centerElement.getDim();
		pre.add(centerNode);
		lockedNodes.add(centerNode);
		frontier.add(centerNode);
		return true;
	}

	// find the edge that is opposite the obtuse angle of the element
	private Edge<Element.Edge> getOpposite(Node<Element, Element.Edge> node) {
		Element element = node.getData();
		Collection<? extends Node<Element, Element.Edge>> neighbors = node.getNeighbors();
		if (neighbors.size() != 3) {
			throw new Error(String.format("neighbors %d", neighbors.size()));
		}
		for (Edge<Element.Edge> edge : node.getEdges()) {
			Element.Edge edgeData = edge.getData();
			if (element.getObtuse().notEquals(edgeData.getPoint(0)) && element.getObtuse().notEquals(edgeData.getPoint(1))) {
				return edge;
			}
		}
		return null;
	}
	
	public boolean isMember(Node<Element, Element.Edge> node) {
		Element element = node.getData();
		return element.inCircle(center);
	}
	
	public boolean build() {
		while (frontier.size() != 0) {
			Node<Element, Element.Edge> curr = frontier.poll();
			Collection<? extends Node<Element, Element.Edge>> neighbors = curr.getNeighbors();
			for (Node<Element, Element.Edge> next : neighbors) {
				if (!tryLock(next)) {
					return false;
				}
				Element nextElement = next.getData();
				Edge<Element.Edge> edge = curr.getEdge(next);
				if ((!(dim == 2 && nextElement.getDim() == 2 && next != centerNode)) && isMember(next)) {
					// isMember says next is part of the cavity, and we're not the second
					// segment encroaching on this cavity
					if ((nextElement.getDim() == 2) && (dim != 2)) { // is segment, and we
						// are encroaching
						unlock();
						return initialize(next) && build();						
					} else {
						if (!pre.contains(next)) {
							pre.add(next);
							frontier.add(next);
						}
					}
				} else { // not a member
					if (!connections.contains(edge)) {
						connections.add(edge);
						pre.addBorder(next);
					}
				}
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public void update() {
		if (centerElement.getDim() == 2) { // we built around a segment
			Element ele1 = new Element(center, centerElement.getPoint(0));
			Node<Element, Element.Edge> node1 = new Node(ele1);
			post.add(node1);
			lock(node1);
			Element ele2 = new Element(center, centerElement.getPoint(1));
			Node<Element, Element.Edge> node2 = new Node(ele2);
			post.add(node2);
			lock(node2);
		}
		for (Edge<Element.Edge> conn : connections) {
			Element.Edge edge = conn.getData();
			Element newElement = new Element(center, edge.getPoint(0), edge.getPoint(1));
			Node<Element, Element.Edge> newNode = new Node(newElement);
			lock(newNode);
			Node<Element, Element.Edge> newConnection;
			if (pre.contains(conn.getNode1())) {
				newConnection = conn.getNode2();
			} else {
				newConnection = conn.getNode1();
			}
			Element.Edge newEdge = newElement.getRelatedEdge(newConnection.getData());
			newNode.addEdge(newConnection, newEdge);
			for (Node<Element, Element.Edge> node : post) {
				Element element = node.getData();
				if (element.isRelated(newElement)) {
					Element.Edge elementEdge = newElement.getRelatedEdge(element);
					newNode.addEdge(node, elementEdge);
				}
			}
			post.add(newNode);
		}
	}
	
	Set<Node> lockedNodes = new SafeSet<>(new HashSet<Node>());
	
	private boolean tryLock(Node n) {
		if (! lockedNodes.contains(n)) {
			boolean result = n.getLock().tryLock();
			if(result) {
				lockedNodes.add(n);
			} else {
				unlock();
			}
			return result;
		}
		return true;
	}

	private void lock(Node n) {
		if (! lockedNodes.contains(n)) {
			n.getLock().lock();
			lockedNodes.add(n);
		}
	}
	
	public void unlock() {
		for (Node n : lockedNodes) {
			n.getLock().unlock();
		}
		lockedNodes.clear();
	}
}
