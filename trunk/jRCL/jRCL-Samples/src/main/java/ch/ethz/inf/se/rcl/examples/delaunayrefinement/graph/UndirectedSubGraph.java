/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import rcl.annotations.Checked;
import rcl.collections.SafeSet;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class UndirectedSubGraph<N, E> extends UndirectedGraph<N, E> {
	private final Set<Node<N, E>> border;
	
	{
		border = new SafeSet<>(new HashSet<Node<N, E>>());
	}

	public boolean existsBorder(Node<N, E> b) {
		return border.contains(b);
	}

	public boolean addBorder(Node<N, E> b) {
		return border.add(b);
	}

	public Set<Node<N, E>> getBorder() {
		return Collections.unmodifiableSet(border);
	}

	public void reset() {
		clear();
		border.clear();
	}
}
