package ch.ethz.inf.se.rcl.examples;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rcl.annotations.Unchecked;

/**
 *
 * @author mschill
 */
@Unchecked
public class RCLExamples {

	private enum ArgumentParsingState {

		name,
		value
	}

	private static class ExampleRun {

		String name;
		int parameter;
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		List<ExampleRun> examplesToRun = new ArrayList<>(args.length);

		ArgumentParsingState state = ArgumentParsingState.name;
		ExampleRun run = null;
		for (String arg : args) {
			switch (state) {
				case name:
					if (arg.equals("-p")) {
						state = ArgumentParsingState.value;
					} else {
						if (run != null) {
							examplesToRun.add(run);
						}
						run = new ExampleRun();
						run.name = arg;
					}
					break;
				case value:
					run.parameter = Integer.parseInt(arg);
					state = ArgumentParsingState.name;
					break;
			}
		}
		if (run != null) {
			examplesToRun.add(run);
		}

		for (ExampleRun r : examplesToRun) {
//		(new PipelineExample()).start();
//		(new PhilosophersExample()).start();
//		(new CarSharingExample()).start();
//		(new MonitorExample()).start();
			Thread t = null;
			switch (r.name) {
				case "pp":
					if (r.parameter > 0) {
						t = new PingPongExample(100, false);
						t.start();
						try {
							t.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(RCLExamples.class.getName()).log(Level.SEVERE, null, ex);
						}
						t = new PingPongExample(r.parameter, true);
					} else {
						t = new PingPongExample();
					}
					break;
				case "sort":
					if (r.parameter > 0) {
						t = new SortingExample(100, false);
						t.start();
						try {
							t.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(RCLExamples.class.getName()).log(Level.SEVERE, null, ex);
						}
						t = new SortingExample(r.parameter, true);
					} else {
						t = new SortingExample(50000, false);
					}
					break;
				case "delref":
					if (r.parameter > 0) {
						t = new DelaunayRefinementExample(4, false);
						t.start();
						try {
							t.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(RCLExamples.class.getName()).log(Level.SEVERE, null, ex);
						}
						t = new DelaunayRefinementExample(r.parameter, true);
					} else {
						t = new DelaunayRefinementExample();
					}
					break;
			}
			if (t != null) {
				System.gc();
				System.gc();
				System.gc();
				System.gc();
				System.gc();
				t.start();
				try {
					t.join();
				} catch (InterruptedException ex) {
					Logger.getLogger(RCLExamples.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}

	}
}
