/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Edge;
import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import rcl.RCLThread;
import rcl.annotations.Checked;
import rcl.collections.SafeList;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class DelaunayRefiner extends RCLThread {

	private final Counter counter;
	private final BlockingQueue<WorkItem> worklist;
	private final Mesh mesh;
	private final String identifier;
	
	public DelaunayRefiner(Counter counter, BlockingQueue<WorkItem> worklist, Mesh mesh, String identifier) {
		this.counter = counter;
		this.worklist = worklist;
		this.mesh = mesh;
		this.identifier = identifier;
	}

	public DelaunayRefiner(Counter counter, BlockingQueue<WorkItem> worklist, Mesh mesh) {
		this.counter = counter;
		this.worklist = worklist;
		this.mesh = mesh;
		this.identifier = super.toString();
	}

	@Override
	public void run() {
		boolean finished = false;
		Cavity cavity = new Cavity(mesh);
		List<WorkItem> newBadElements = new SafeList<>(new ArrayList<WorkItem>());

		while (!worklist.isEmpty() && !finished) {
			try {
				WorkItem item = worklist.take();
				Node<Element, Element.Edge> badElement = item.getData();
				if (badElement == null)
					return;

				boolean fixit;
				synchronized (mesh) {
					fixit = (badElement != null) && (mesh.contains(badElement));
				}
				if (fixit) {
					if (cavity.initialize(badElement) && cavity.build()) {
						cavity.update();
						// remove the old data
						for (Node<Element, Element.Edge> node : cavity.getPre()) {
							node.clearEdges();
						}
						synchronized (mesh) {
							for (Node<Element, Element.Edge> node : cavity.getPre()) {
								mesh.remove(node);
							}
							// add new data
							for (Node<Element, Element.Edge> node : cavity.getPost()) {
								mesh.add(node);
							}
							if (mesh.contains(badElement)) {
								newBadElements.add(new WorkItem(badElement));
							}
						}
						newBadElements.addAll(WorkItem.itemCollection(cavity.getPost().newBad()));
					} else {
						newBadElements.add(item);
					}
					cavity.unlock();
				}

				worklist.addAll(newBadElements);
				counter.changeCounter(newBadElements.size() - 1);
				newBadElements.clear();
			} catch (InterruptedException ex) {
				finished = true;
			}
		}
	}

	@Override
	public String toString() {
		return identifier;
	}
}
