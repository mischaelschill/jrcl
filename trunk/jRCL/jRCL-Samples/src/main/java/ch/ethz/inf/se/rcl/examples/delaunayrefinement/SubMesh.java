/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package ch.ethz.inf.se.rcl.examples.delaunayrefinement;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.Node;
import ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph.UndirectedSubGraph;
import java.util.HashSet;
import java.util.Set;
import rcl.annotations.Checked;
import rcl.collections.SafeSet;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class SubMesh extends UndirectedSubGraph<Element, Element.Edge> {

	public Set<Node<Element, Element.Edge>> newBad() {
		Set<Node<Element, Element.Edge>> result = new SafeSet<>(new HashSet<Node<Element, Element.Edge>>());
		for (Node<Element, Element.Edge> node : this) {
			Element element = node.getData();
			if (element.isBad()) {
				result.add(node);
			}
		}
		return result;
	}	
}
