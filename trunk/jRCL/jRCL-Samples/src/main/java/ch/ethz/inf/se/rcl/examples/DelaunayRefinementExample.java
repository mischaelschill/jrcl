/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples;

import ch.ethz.inf.se.rcl.examples.delaunayrefinement.ParallelDelaunayRefinement;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rcl.annotations.Checked;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class DelaunayRefinementExample extends Thread {

	private final int numWorkers;
	private final boolean measureTimes;

	public DelaunayRefinementExample(int numWorkers, boolean measureTimes) {
		this.numWorkers = numWorkers;
		this.measureTimes = measureTimes;
	}

	public DelaunayRefinementExample() {
		this.numWorkers = 4;
		this.measureTimes = false;
	}
	
	@Override
	public void run() {
		try {
			ParallelDelaunayRefinement pdr = new ParallelDelaunayRefinement();
			pdr.run("250k.2", numWorkers, measureTimes);
		} catch (InterruptedException ex) {
			Logger.getLogger(DelaunayRefinementExample.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(DelaunayRefinementExample.class.getName()).log(Level.SEVERE, null, ex);
			try {
				System.err.println("Current working directory: " + new File(".").getCanonicalPath());
			} catch (IOException ex1) {
				Logger.getLogger(DelaunayRefinementExample.class.getName()).log(Level.SEVERE, null, ex1);
			}
		}
	}
}
