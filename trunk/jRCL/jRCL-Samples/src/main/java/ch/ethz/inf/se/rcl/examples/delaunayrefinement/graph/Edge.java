/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph;

import java.util.Objects;
import rcl.annotations.Checked;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class Edge<D> {
	private final Node node1, node2;
	private final D data;
	
	public Edge(Node node1, Node node2) {
		this.node1 = node1;
		this.node2 = node2;
		this.data = null;
	}
	
	public Edge(Node node1, Node node2, D data) {
		this.node1 = node1;
		this.node2 = node2;
		this.data = data;
	}

	public Node getNode1() {
		return node1;
	}

	public Node getNode2() {
		return node2;
	}

	public D getData() {
		return data;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 37 * hash + Objects.hashCode(this.node1);
		hash = 37 * hash + Objects.hashCode(this.node2);
		hash = 37 * hash + Objects.hashCode(this.data);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Edge<?> other = (Edge<?>) obj;
		if (!Objects.equals(this.node1, other.node1)) {
			return false;
		}
		if (!Objects.equals(this.node2, other.node2)) {
			return false;
		}
		if (!Objects.equals(this.data, other.data)) {
			return false;
		}
		return true;
	}
}
