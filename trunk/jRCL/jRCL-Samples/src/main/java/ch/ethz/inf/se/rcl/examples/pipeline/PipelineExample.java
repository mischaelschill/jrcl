package ch.ethz.inf.se.rcl.examples.pipeline;


import rcl.util.concurrent.Queues;
import rcl.annotations.Checked;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mschill
 */
@Checked
public class PipelineExample extends Thread {

	@Override
	public void run() {
		System.out.println("Pipeline example: Counting from 11 to 20");
		final BlockingQueue<WorkItem> in = Queues.fromBlockingQueue(new ArrayBlockingQueue<WorkItem>(2));
		BlockingQueue<WorkItem> input = in;
		BlockingQueue<WorkItem> output = Queues.fromBlockingQueue(new ArrayBlockingQueue<WorkItem>(2));
		Stage p = new Stage(input, output);
		p.setDaemon(true);
		p.start();
		for (int i = 0; i < 10; i++) {
			input = output;
			output = Queues.fromBlockingQueue(new ArrayBlockingQueue<WorkItem>(2));
			p = new Stage(input, output);
			p.setDaemon(true);
			p.start();
		}
		final BlockingQueue<WorkItem> out = output;
		for (int i = 0; i < 10; i++) {
			final WorkItem wi = new WorkItem();
			wi.setData(i);
			try {
				in.put(wi);
			} catch (InterruptedException ex) {
				Logger.getLogger(PipelineExample.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		for (int i = 0; i < 10; i++) {
			final WorkItem wi;
			try {
				wi = out.take();
				System.err.println(wi.getData());
			} catch (InterruptedException ex) {
				Logger.getLogger(PipelineExample.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
