package ch.ethz.inf.se.rcl.examples;

import rcl.annotations.Checked;
import rcl.collections.SafeList;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mischael
 */
@Checked
public class SortingExample extends Thread {

	private final int length;
	private final boolean measureTime;

	public SortingExample(int length, boolean measureTime) {
		this.length = length;
		this.measureTime = measureTime;
	}

	@Override
	public void run() {
		Random rand = new Random(42);
		SafeList<Double> ar = new SafeList<>(new ArrayList<Double>());
		for (int i = 0; i < length; i++) {
			ar.add(rand.nextDouble());
		}
		Quicksort<Double> q = new Quicksort<>();
		System.gc();
		long start = System.currentTimeMillis();
		q.sort(ar);
		long stop = System.currentTimeMillis();
		if (measureTime) {
			System.out.println(stop - start);
		}
	}

}

@Checked
class Quicksort<T extends Comparable> {

	private SafeList<T> data;
	private int size;

	public void sort(SafeList<T> values) {
		// Check for empty or null array
		if (values == null || values.isEmpty()) {
			return;
		}
		this.data = values;
		size = values.size();
		quicksort(0, size - 1);
	}

	private void quicksort(int low, int high) {
		int i = low, j = high;
		T pivot = data.get(low + (high - low) / 2);

		while (i <= j) {
			while (data.get(i).compareTo(pivot) < 0) {
				i++;
			}
			while (data.get(j).compareTo(pivot) > 0) {
				j--;
			}

			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}
		if (low < j) {
			quicksort(low, j);
		}
		if (i < high) {
			quicksort(i, high);
		}
	}

	private void exchange(int i, int j) {
		T temp = data.get(i);
		data.set(i, data.get(j));
		data.set(j, temp);
	}
}
