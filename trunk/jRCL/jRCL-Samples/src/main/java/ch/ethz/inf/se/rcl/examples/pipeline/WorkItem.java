package ch.ethz.inf.se.rcl.examples.pipeline;

import rcl.annotations.Checked;

/**
 *
 * @author mschill
 */
@Checked
public class WorkItem {

	private int data;

	/**
	 * @return some data contained in the item
	 */
	public int getData() {
		return data;
	}

	/**
	 * @param data some data contained in the item
	 */
	public void setData(int data) {
		this.data = data;
	}
}
