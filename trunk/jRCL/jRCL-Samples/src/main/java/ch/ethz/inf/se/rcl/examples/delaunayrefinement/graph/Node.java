/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples.delaunayrefinement.graph;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import rcl.annotations.Checked;
import rcl.collections.SafeSet;
import rcl.internal.Resource;
import rcl.util.concurrent.ResourceLock;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class Node<D, E> implements Iterable<Edge<E>> {

	private final ResourceLock<Node<D, E>> lock;
	private final SafeSet<Edge<E>> edges = new SafeSet<>(new HashSet<Edge<E>>());
	private D data;

	public Node() {
		this(null);
	}

	public Node(D data) {
		this.data = data;
		lock = new ResourceLock<Node<D, E>>(this);
	}

	public ResourceLock<Node<D, E>> getLock() {
		return lock;
	}

	public Set<Edge<E>> getEdges() {
		return Collections.unmodifiableSet(edges);
	}

	public Edge<E> addEdge(Node<D, E> other, E data) {
		Edge e = new Edge(this, other, data);
		edges.add(e);
		other.edges.add(e);
		return e;
	}

	public void removeEdge(Edge<E> e) {
		assert e.getNode1() == this || e.getNode2() == this;
		edges.remove(e);
		if (e.getNode1() == this) {
			e.getNode2().edges.remove(e);
		} else {
			e.getNode1().edges.remove(e);
		}
	}

	public void clearEdges() {
		for (Edge<E> e : edges) {
			if (e.getNode1() == this) {
				e.getNode2().edges.remove(e);
			} else {
				e.getNode1().edges.remove(e);
			}
		}
		edges.clear();
	}

	public D getData() {
		return data;
	}

	public void setData(D data) {
		this.data = data;
	}

	public Set<Node<D, E>> getNeighbors() {
		Set<Node<D, E>> neighbors = new SafeSet<>(new HashSet<Node<D, E>>());
		for (Edge<E> e : edges) {
			if (e.getNode1() == this) {
				neighbors.add(e.getNode2());
			} else {
				neighbors.add(e.getNode1());
			}
		}
		return neighbors;
	}

	public Edge<E> getEdge(Node<D, E> neighbor) {
		for (Edge<E> e : edges) {
			if (e.getNode1() == neighbor || e.getNode2() == neighbor) {
				return e;
			}
		}
		return null;
	}

	@Override
	public Iterator<Edge<E>> iterator() {
		return edges.iterator();
	}
}
