package ch.ethz.inf.se.rcl.examples;

import rcl.util.concurrent.ResourceLock;
import rcl.annotations.Checked;
import rcl.collections.SafeList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mischael
 */
@Checked
public class PhilosophersExample extends Thread{
	private int numPhilosophers = 20;

	public PhilosophersExample() {;
	}

	public PhilosophersExample(int numPhilosophers) {
		this.numPhilosophers = numPhilosophers;
	}

	
	@Override
	public void run() {
		//Loading classes so that the transformation time is not counted
		new Philosopher(new ResourceLock<>(new Fork()), new ResourceLock<>(new Fork()), 0);
		
		long start = System.currentTimeMillis();
		List<Philosopher> philosophers = new SafeList<>(new LinkedList<Philosopher>());
		Fork f = new Fork();
		ResourceLock<Fork> firstFork = new ResourceLock<>(f);
		f = new Fork();
		ResourceLock<Fork> leftFork = new ResourceLock<>(f);
		//The first Philosophers take the forks the other way
		philosophers.add(new Philosopher(leftFork, firstFork, 0));
		for (int i = 1; i < numPhilosophers - 1; i++) {
			ResourceLock<Fork> rightFork = new ResourceLock<>(new Fork());
			philosophers.add(new Philosopher(leftFork, rightFork, i));
			leftFork = rightFork;
		}
		philosophers.add(new Philosopher(leftFork, firstFork, numPhilosophers - 1));
		for (Philosopher p : philosophers) {
			p.start();
		}
		for (Philosopher p : philosophers) {
			try {
				p.join();
			} catch (InterruptedException ex) {
				Logger.getLogger(PhilosophersExample.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println(numPhilosophers + "," + (end - start));
	}
}

@Checked
class Philosopher extends Thread {
	private final ResourceLock<Fork> left, right;
	private final int iterations = 10;
	private final Random random;
	
	public Philosopher (ResourceLock<Fork> left, ResourceLock<Fork> right, long number) {
		this.left = left; this.right = right;
		random = new Random(number);
	}
	
	public void run () {
		for (int i = 0; i < iterations; i++) {
			think(); eat();
		}
	}
	
	private void eat () {
		left.lock (); right.lock ();
		left.getContent().use(); right.getContent().use();
		try {
			Thread.sleep (random.nextInt(1000));
		} catch (InterruptedException e) {}
		left.unlock (); right.unlock ();
	}
	
	private void think () {
		try {
			Thread.sleep (random.nextInt(1000));
		} catch (InterruptedException e) {}
	}
}

@Checked
class Fork {
	private long wear;
	
	public void use() {
		wear++;
	}
}