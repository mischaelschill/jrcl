package ch.ethz.inf.se.rcl.examples.pipeline;

import rcl.annotations.Checked;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A pipeline stage. Does some work on a package.
 *
 * @author mschill
 */
@Checked
public class Stage extends Thread {

	private final BlockingQueue<WorkItem> input, output;

	public Stage(BlockingQueue<WorkItem> input, BlockingQueue<WorkItem> output) {
		this.input = input;
		this.output = output;
	}

	@Override
	public void run() {
		while (true) {
			WorkItem p = null;
			int data = 0;
			try {
				p = input.take();
			} catch (InterruptedException ex) {
				continue;
			}
			data = p.getData();
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex) {
			}
			p.setData(data + 1);
			try {
				output.put(p);
			} catch (InterruptedException ex) {
				break;
			}
		}
	}
}
