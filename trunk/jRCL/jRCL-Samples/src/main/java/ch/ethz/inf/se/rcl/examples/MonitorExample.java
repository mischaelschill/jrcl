/*
 * The MIT License
 *
 * Copyright 2013 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ch.ethz.inf.se.rcl.examples;

import rcl.annotations.Checked;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
@Checked
public class MonitorExample extends Thread {

	@Override
	public void run() {
		SomeResource r = new SomeResource();
		for (int i = 0; i < 20; i++) {
			(new Worker(r)).start();
		}
	}
}

class Worker extends Thread {

	private final SomeResource resource;

	public Worker(SomeResource resource) {
		this.resource = resource;
	}

	@Override
	public void run() {
		for (int i = 0; i < 5000; i++) {
			try {
				resource.doWork();
				Thread.sleep(20);
				resource.doMoreWork();
				Thread.sleep(20);
				synchronized (resource) {
					resource.doALotOfWork();
				}
				Thread.sleep(20);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}

class SomeResource {

	private int workDone = 0;

	public void doWork() {
		synchronized (this) {
			workDone++;
		}
	}

	public synchronized void doMoreWork() {
		workDone+=2;
	}

	public void doALotOfWork() {
		workDone+=4;
	}

}
