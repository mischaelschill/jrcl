package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author mischael
 * @param <T> The type of the object to protect
 */
@AbstractProcess
public class LightLock<T> implements Lock {

	private final T content;
	private final Semaphore lock = new Semaphore(1);

	@SuppressWarnings("LeakingThisInConstructor")
	public LightLock(T content) {
		assert content != null;
		this.content = content;
	}

	/**
	 * @return the object that is protected by the lock
	 */
	public T getContent() {
		return content;
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lock() {
		lock.acquireUninterruptibly();
	}

	/**
	 * Releases the lock: Transfers control of the content to the lock
	 */
	@Override
	public void unlock() {
		RCL.pass(content, this);
		lock.release();
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lockInterruptibly() throws InterruptedException {
		lock.acquire();
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock() {
		boolean retval = lock.tryAcquire();
		return retval;
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @param time the number of time units to wait until giving up
	 * @param unit the time unit
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		boolean retval = lock.tryAcquire(time, unit);
		return retval;
	}

	@Override
	public Condition newCondition() {
		throw new UnsupportedOperationException("Not supported by light locks.");
	}
}
