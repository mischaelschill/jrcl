package rcl.util.concurrent;

import rcl.annotations.AbstractProcess;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author mischael
 * @param <T> The type of the object to protect
 */
@AbstractProcess
public class ResourceLock<T> implements Lock {

	private final T content;
	private final ReentrantLock lock = new ReentrantLock();

	@SuppressWarnings("LeakingThisInConstructor")
	public ResourceLock(T content) {
		assert content != null;
		this.content = content;
	}

	/**
	 * @return the object that is protected by the lock
	 */
	public T getContent() {
		return content;
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lock() {
		lock.lock();
	}

	/**
	 * Releases the lock: Transfers control of the content to the lock
	 */
	@Override
	public void unlock() {		lock.unlock();
	}

	/**
	 * Acquires the lock: Transfers control of the content to the current thread
	 */
	@Override
	public void lockInterruptibly() throws InterruptedException {
		lock.lockInterruptibly();
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock() {
		boolean retval = lock.tryLock();
		return retval;
	}

	/**
	 * Tries to acquire the lock: Transfers control of the content to the
	 * current thread if successful
	 *
	 * @param time the number of time units to wait until giving up
	 * @param unit the time unit
	 * @return whether the lock was acquired
	 */
	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		boolean retval = lock.tryLock(time, unit);
		return retval;
	}

	public boolean isHeldByCurrentThread() {
		return lock.isHeldByCurrentThread();
	}

	public boolean __isWriteable() {
		return isHeldByCurrentThread();
	}

	public boolean __isReadable() {
		return __isWriteable();
	}

	@AbstractProcess
	class CheckedCondition implements Condition {

		private final Condition cond = lock.newCondition();

		@Override
		public void await() throws InterruptedException {
			cond.await();
		}

		@Override
		public void awaitUninterruptibly() {
			cond.awaitUninterruptibly();
		}

		@Override
		public long awaitNanos(long nanosTimeout) throws InterruptedException {
			long retval = cond.awaitNanos(nanosTimeout);
			return retval;
		}

		@Override
		public boolean await(long time, TimeUnit unit) throws InterruptedException {
			boolean retval = cond.await(time, unit);
			return retval;
		}

		@Override
		public boolean awaitUntil(Date deadline) throws InterruptedException {
			boolean retval = cond.awaitUntil(deadline);
			return retval;
		}

		@Override
		public void signal() {
			cond.signal();
		}

		@Override
		public void signalAll() {
			cond.signalAll();
		}
	}

	/**
	 * @return a new condition variable
	 */
	@Override
	public Condition newCondition() {

		return new CheckedCondition();
	}
}
