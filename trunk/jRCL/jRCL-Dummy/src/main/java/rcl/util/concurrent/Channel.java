package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A simple synchronous channel
 *
 * @author mischael
 * @param <>> The type of message to be transferred through this channel
 */
@AbstractProcess
public class Channel<T> {

	private T message;
	private final ReentrantLock l = new ReentrantLock(true);
	private final Condition channelEmpty = l.newCondition();
	private final Condition messageAvailable = l.newCondition();
	private final Condition channelCleared = l.newCondition();

	public void send(T t) {
		RCL.pass(t, this);
		l.lock();
		try {
			while (message != null) {
				channelEmpty.awaitUninterruptibly();
			}
			message = t;
			messageAvailable.signal();
			while (message == t) {
				channelCleared.awaitUninterruptibly();
			}
		} finally {
			l.unlock();
		}
	}

	public T receive() {
		l.lock();
		T t = null;
		try {
			while (message == null) {
				messageAvailable.awaitUninterruptibly();
			}
			t = message;
			message = null;
			channelCleared.signalAll();
			channelEmpty.signal();
			RCL.pass(t, Thread.currentThread());
		} finally {
			l.unlock();
		}
		return t;
	}

	public boolean __isWriteable() {
		return l.isHeldByCurrentThread();
	}
	
	public boolean __isReadable() {
		return __isWriteable();
	}
}
