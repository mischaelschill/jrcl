package rcl.util.concurrent;

import rcl.RCL;
import rcl.annotations.AbstractProcess;
import rcl.annotations.Shared;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author mischael
 */
@AbstractProcess
public final class ReadWriteLock<T> {

	private final SharedBox<T> reader;
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	@SuppressWarnings("LeakingThisInConstructor")
	public ReadWriteLock(T content) {
		reader = new SharedBox<T>(content);
	}

	public T getContent() {
		return reader.getContent();
	}

	public void lockWrite() {
		lock.writeLock().lock();
	}

	public void unlockWrite() {
		RCL.pass(reader, this);
		lock.writeLock().unlock();
	}

	public void lockRead() {
		lock.readLock().lock();
	}

	public void unlockRead() {
		lock.readLock().unlock();
	}

	public boolean __isWriteable() {
		return lock.writeLock().isHeldByCurrentThread();
	}

	public boolean __isReadable() {
		return __isWriteable();
	}
}

@Shared
class SharedBox<T> {

	private final T content;

	public SharedBox(T content) {
		this.content = content;
	}

	public T getContent() {
		return content;
	}
}
