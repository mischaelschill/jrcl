package rcl;

/**
 * This class is used for the RCL transformation and should not be used 
 * directly, is it is not available in the dummy library.
 * 
 * @author mschill
 */
public class RCL {
	public static void pass(Object resource, Object controller) {
	}
}
