package rcl.collections;

import rcl.annotations.Checked;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author mischael
 */
@Checked
public class SafeList<T> implements List<T> {

	private final List<T> original;

	public SafeList(List<T> original) {
		this.original = original;
	}

	@Override
	public int size() {
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<>(original.iterator());
	}

	@Override
	public Object[] toArray() {
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return original.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		return original.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		original.clear();
	}

//	@Override
//	public boolean equals(Object o) {
//		if (o instanceof SafeList) {
//			SafeList t = (SafeList) o;
//			if (original == null) {
//				return t.original == null;
//			}
//			return original.equals(t.original);
//		} else {
//			return false;
//		}
//	}

//	@Override
//	public int hashCode() {
//		if (original == null) {
//			return super.hashCode();
//		}
//		return original.hashCode();
//	}

	@Override
	public T get(int index) {
		return original.get(index);
	}

	@Override
	public T set(int index, T element) {
		return original.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		original.add(index, element);
	}

	@Override
	public T remove(int index) {
		return original.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return original.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return original.lastIndexOf(o);
	}

	@Override
	public SafeListIterator<T> listIterator() {
		return new SafeListIterator<>(original.listIterator());
	}

	@Override
	public SafeListIterator<T> listIterator(int index) {
		return new SafeListIterator<>(original.listIterator(index));
	}

	@Override
	public SafeList<T> subList(int fromIndex, int toIndex) {
		return new SafeList<>(original.subList(fromIndex, toIndex));
	}

//	@Override
//	public String toString() {
//		if (original == null) {
//			return super.toString();
//		}
//		return original.toString();
//	}

}
