package rcl.collections;

import rcl.annotations.Checked;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author mischael
 */
@Checked
class SafeSet<T> implements Set<T> {
	private final Set<T> original;

	SafeSet(Set<T> original) {
		this.original = original;
	}

	@Override
	public int size() {
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<T>(original.iterator());
	}

	@Override
	public Object[] toArray() {
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return original.addAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return original.retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return original.removeAll(c);
	}

	@Override
	public void clear() {
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		return original.equals(o);
	}

	@Override
	public int hashCode() {
		return original.hashCode();
	}

	@Override
	public String toString() {
		return original.toString();
	}
	
}
