package rcl.collections;

import rcl.annotations.Checked;
import java.util.ListIterator;

/**
 *
 * @author mischael
 */
@Checked
class SafeListIterator<T> implements ListIterator<T> {
	private final ListIterator<T> original;

	public SafeListIterator(ListIterator<T> original) {
		this.original = original;
	}

	@Override
	public boolean hasNext() {
		return original.hasNext();
	}

	@Override
	public T next() {
		return original.next();
	}

	@Override
	public boolean hasPrevious() {
		return original.hasPrevious();
	}

	@Override
	public T previous() {
		return original.previous();
	}

	@Override
	public int nextIndex() {
		return original.nextIndex();
	}

	@Override
	public int previousIndex() {
		return original.previousIndex();
	}

	@Override
	public void remove() {
		original.remove();
	}

	@Override
	public void set(T e) {
		original.set(e);
	}

	@Override
	public void add(T e) {
		original.add(e);
	}

	@Override
	public int hashCode() {
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return original.equals(obj);
	}

	@Override
	public String toString() {
		return original.toString();
	}
	
}
