/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rcl.collections;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import rcl.RCL;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SafeDeque<T> implements Deque<T> {
	private final Deque<T> original;
	
	public SafeDeque (Deque<T> original) {
		this.original = original;
	}

	public SafeDeque (Deque<T> original, Object source) {
		this.original = original;
	}

	@Override
	public void addFirst(T e) {
		original.addFirst(e);
	}

	@Override
	public void addLast(T e) {
		original.addLast(e);
	}

	@Override
	public boolean offerFirst(T e) {
		return original.offerFirst(e);
	}

	@Override
	public boolean offerLast(T e) {
		return original.offerLast(e);
	}

	@Override
	public T removeFirst() {
		return original.removeFirst();
	}

	@Override
	public T removeLast() {
		return original.removeLast();
	}

	@Override
	public T pollFirst() {
		return original.pollFirst();
	}

	@Override
	public T pollLast() {
		return original.pollLast();
	}

	@Override
	public T getFirst() {
		return original.getFirst();
	}

	@Override
	public T getLast() {
		return original.getLast();
	}

	@Override
	public T peekFirst() {
		return original.peekFirst();
	}

	@Override
	public T peekLast() {
		return original.peekLast();
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		return original.removeFirstOccurrence(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		return original.removeLastOccurrence(o);
	}

	@Override
	public boolean add(T e) {
		return original.add(e);
	}

	@Override
	public boolean offer(T e) {
		return original.offer(e);
	}

	@Override
	public T remove() {
		return original.remove();
	}

	@Override
	public T poll() {
		return original.poll();
	}

	@Override
	public T element() {
		return original.element();
	}

	@Override
	public T peek() {
		return original.peek();
	}

	@Override
	public void push(T e) {
		original.push(e);
	}

	@Override
	public T pop() {
		return original.pop();
	}

	@Override
	public boolean remove(Object o) {
		return original.remove(o);
	}

	@Override
	public boolean contains(Object o) {
		return original.contains(o);
	}

	@Override
	public int size() {
		return original.size();
	}

	@Override
	public Iterator<T> iterator() {
		return original.iterator();
	}

	@Override
	public Iterator<T> descendingIterator() {
		return original.descendingIterator();
	}

	@Override
	public boolean isEmpty() {
		return original.isEmpty();
	}

	@Override
	public Object[] toArray() {
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return original.toArray(a);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		original.clear();
	}

	@Override
	public int hashCode() {
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SafeDeque) {
			return original.equals(((SafeDeque)obj).original);
		} else return original.equals(obj);
	}

	@Override
	public String toString() {
		return original.toString();
	}
}
