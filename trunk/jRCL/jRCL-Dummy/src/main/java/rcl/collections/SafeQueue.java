package rcl.collections;

import rcl.annotations.Checked;
import java.util.Collection;
import java.util.Queue;
import rcl.RCL;
import rcl.internal.Resource;

/**
 *
 * @author mischael
 */
@Checked
public class SafeQueue<T> implements Queue<T> {

	private final Queue<T> original;
	private final Object source;

	public SafeQueue(Queue<T> original) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	public SafeQueue(Queue<T> original, Object source) {
		this.original = original;
		this.source = source;
	}	
	
	@Override
	public int size() {
		RCL.checkRead(source);
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		RCL.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		RCL.checkRead(source);
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		RCL.checkRead(source);
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Object[] toArray() {
		RCL.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		RCL.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		RCL.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		RCL.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		RCL.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		RCL.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		RCL.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		RCL.checkWrite(source);
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		RCL.checkRead(source);
		RCL.checkRead(o);
		if (o instanceof SafeQueue) {
			SafeQueue t = (SafeQueue) o;
			RCL.checkRead(t.original);
			if (original == null) {
				return t.original == null;
			}
			return original.equals(t.original);
		} else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		RCL.checkRead(source);
		if (original == null) {
			return super.hashCode();
		}
		return original.hashCode();
	}
	@Override
	public String toString() {
		RCL.checkRead(source);
		if (original == null) {
			return super.toString();
		}
		return original.toString();
	}
	
	@Override
	public boolean offer(T e) {
		RCL.checkWrite(source);
		return original.offer(e);
	}

	@Override
	public T remove() {
		RCL.checkWrite(source);
		return original.remove();
	}

	@Override
	public T poll() {
		RCL.checkWrite(source);
		return original.poll();
	}

	@Override
	public T element() {
		RCL.checkRead(source);
		return original.element();
	}

	@Override
	public T peek() {
		RCL.checkRead(source);
		return original.peek();
	}	
}
