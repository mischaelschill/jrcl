package rcl.collections;

import rcl.annotations.Checked;
import java.util.Iterator;

/**
 *
 * @author mischael
 */
@Checked
public class SafeIterator<T> implements Iterator<T> {
	private final Iterator<T> original;

	public SafeIterator(Iterator<T> original) {
		this.original = original;
	}

	@Override
	public boolean hasNext() {
		return original.hasNext();
	}

	@Override
	public T next() {
		return original.next();
	}

	@Override
	public void remove() {
		original.remove();
	}
}
